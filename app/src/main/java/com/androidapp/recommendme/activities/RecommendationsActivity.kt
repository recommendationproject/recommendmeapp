package com.androidapp.recommendme.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.recommendme.R
import com.androidapp.recommendme.User
import com.androidapp.recommendme.fragments.RecommendationFragment
import kotlinx.android.synthetic.main.toolbar.*

class RecommendationsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recommendations)

        setUpToolbar()

        val fm = supportFragmentManager
        var fragment = fm.findFragmentById(R.id.fragment_container)

        // ensures fragments already created will not be created
        if (fragment == null) {
            fragment = RecommendationFragment()
            // create and commit a fragment transaction
            fm.beginTransaction()
                .add(R.id.fragment_container, fragment)
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .commit()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            val user = data.getSerializableExtra("user") as User
            intent.putExtra("user", user)
            setUpToolbar()
        }
    }

    private fun setUpToolbar() {
        var extras = intent.extras
        var user = extras?.getSerializable("user") as User
        var recommendationsType = extras.getString("recommendations_type")

        if (recommendationsType == "bookmarks") {
            toolbar_title.text = "Bookmarks"
            toolbar_left_button.setImageResource(R.drawable.arrow_back)
            toolbar_left_button.setOnClickListener {
                onBackPressed()
            }
        } else {
            toolbar_title.text = "Recommendations"
            toolbar_left_button.setImageResource(R.drawable.profile_button_image)
            toolbar_left_button.setOnClickListener {
                var intent = Intent(this, UserProfileActivity::class.java)
                intent.putExtra("user", user)
                startActivityForResult(intent, 1)
            }
        }
    }
}
