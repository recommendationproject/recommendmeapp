package com.androidapp.recommendme.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.core.view.size
import com.example.recommendme.*
import com.facebook.AccessToken
import kotlinx.android.synthetic.main.activity_movie_profile.*
import kotlinx.android.synthetic.main.activity_movie_profile.movie_interest_rating_bar
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONObject
import com.androidapp.recommendme.CheckboxItem
import com.androidapp.recommendme.MovieProfile
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.User
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable


class MovieProfileActivity : AppCompatActivity() {

    private var genreList: MutableList<CheckboxItem> = mutableListOf()
    private var languageList: MutableList<CheckboxItem> = mutableListOf()
    private var yearList: MutableList<CheckboxItem> = mutableListOf()
    private var streamingPlatformList:MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_profile)

        // Get intent details
        val extras = intent.extras
        var user = extras?.getSerializable("user") as User
        val userId = user.userId
        var viaProfile = extras?.getString("viaProfile")

        var movieProfile = user.movieProfile
        Log.d(this.localClassName, movieProfile.toString())

        //setup toolbar
        toolbar_title.text = "Setup your movie profile"
        if (viaProfile == "true") {
            toolbar_left_button.setImageResource(R.drawable.arrow_back)
            toolbar_left_button.setOnClickListener {
                onBackPressed()
            }
        }

        genreList = movieProfile.genreList

        for (genre in genreList) {
            val genreChip = Chip(this)
            genreChip.setChipDrawable(ChipDrawable.createFromResource(this, R.xml.movie_profile_chip))
            genreChip.isCheckedIconVisible = false
            genreChip.isCloseIconVisible = false
            genreChip.text = genre.text
            if (genre.isSelected) {
                genreChip.isChecked = true
            }
            movie_profile_genre_group.addView(genreChip)
        }

        languageList = movieProfile.languageList
        for (language in languageList) {
            val languageChip = Chip(this)
            languageChip.setChipDrawable(ChipDrawable.createFromResource(this, R.xml.movie_profile_chip))
            languageChip.isCheckedIconVisible = false
            languageChip.isCloseIconVisible = false
            languageChip.text = language.text
            if (language.isSelected) {
                languageChip.isChecked = true
            }
            movie_profile_language_group.addView(languageChip)
        }

        yearList = movieProfile.yearList
        for (year in yearList) {
            val yearChip = Chip(this)
            yearChip.setChipDrawable(ChipDrawable.createFromResource(this, R.xml.movie_profile_chip))
            yearChip.isCheckedIconVisible = false
            yearChip.isCloseIconVisible = false
            yearChip.text = year.text
            if (year.isSelected) {
                yearChip.isChecked = true
            }
            movie_profile_year_group.addView(yearChip)
        }

        //setup movie interest rating bar
        setupMovieInterestRatingSection(movieProfile)

        // Stream options radio selection
        if (movieProfile.streamingPlatformList.size > 0 ) {
            streaming_pref_radio.isChecked = true
            stream_options_chip_group.visibility = View.VISIBLE

            if ("Netflix" in movieProfile.streamingPlatformList) {
                netflix_option_chip.isChecked = true
            }
            if ("Hotstar" in movieProfile.streamingPlatformList) {
                hotstar_option_chip.isChecked = true
            }
            if ("Prime Video" in movieProfile.streamingPlatformList) {
                amzn_prime_option_chip.isChecked = true
            }
            if ("Zee5" in movieProfile.streamingPlatformList) {
                zee5_option_chip.isChecked = true
            }
            if ("Youtube Movies" in movieProfile.streamingPlatformList) {
                youtube_option_chip.isChecked = true
            }
            if ("Google Play" in movieProfile.streamingPlatformList) {
                gplay_option_chip.isChecked = true
            }
        }

        stream_pref_radio_group.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { stream_pref_radio_group, checkedId ->
                if (no_streaming_pref_radio.isChecked) {
                    stream_options_chip_group.visibility = View.GONE
                }
                if (streaming_pref_radio.isChecked) {
                    stream_options_chip_group.visibility = View.VISIBLE
                }
            }
        )

        // Surprise recommendations
        if (movieProfile.sendSurpriseRecommendation) {
            surprise_reco_check.isChecked = true
        }

        // Update on click
        update_movie_profile.setOnClickListener {
            setUserMovieProfileDetails(userId)
        }
    }

    override fun onBackPressed() {
        var extras = intent.extras
        var user = extras?.getSerializable("user") as User

//        updateSelectedLists()
//
//        if (movie_interest_rating_bar.progress != user.movieProfile.movieInterestRating) {
//            val requestObject = JSONObject()
//            requestObject.put("movie_interest_rating", movie_interest_rating_bar.progress)
//            requestObject.put("genres", getSelectedList(genreList))
//            requestObject.put("languages", getSelectedList(languageList))
//            requestObject.put("years", getSelectedList(yearList))
//
//            user.movieProfile.genreList = genreList
//            user.movieProfile.languageList = languageList
//            user.movieProfile.yearList = yearList
//            user.movieProfile.movieInterestRating = movie_interest_rating_bar.progress
//            intent.putExtra("user", user)
//
//            ServiceVolley().post("/user/movie/profile?userid=" + user.userId + "&access_token=" + AccessToken.getCurrentAccessToken().token, requestObject) { response ->
//                Log.d(this.localClassName, response.toString())
//                // show toast - both success and failure
//            }
//        }

        var returnIntent = Intent()
        returnIntent.putExtra("user", user)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    private fun updateSelectedLists() {
        for (i in 0 until movie_profile_genre_group.size) {
            genreList[i].isSelected = (movie_profile_genre_group.getChildAt(i) as Chip).isChecked
        }

        for (i in 0 until movie_profile_language_group.size) {
            languageList[i].isSelected = (movie_profile_language_group.getChildAt(i) as Chip).isChecked
        }

        for (i in 0 until movie_profile_year_group.size) {
            yearList[i].isSelected = (movie_profile_year_group.getChildAt(i) as Chip).isChecked
        }

        if (streaming_pref_radio.isChecked) {
            if (netflix_option_chip.isChecked) {
                streamingPlatformList.add("Netflix")
            }
            if (hotstar_option_chip.isChecked) {
                streamingPlatformList.add("Hotstar")
            }
            if (amzn_prime_option_chip.isChecked) {
                streamingPlatformList.add("Prime Video")
            }
            if (youtube_option_chip.isChecked) {
                streamingPlatformList.add("Youtube Movies")
            }
            if (gplay_option_chip.isChecked) {
                streamingPlatformList.add("Google Play")
            }
        }
    }

    private fun getSelectedList(itemList: MutableList<CheckboxItem>): JSONArray {
        var selectedItemsList = JSONArray()
        for (item in itemList) {
            if (item.isSelected) {
                selectedItemsList.put(item.property)
            }
        }
        return selectedItemsList
    }

    private fun getJSONArrayFromList(itemList: MutableList<String>): JSONArray {
        var selectedItemsList = JSONArray()
        for (item in itemList) {
                selectedItemsList.put(item)
        }
        return selectedItemsList
    }

    private fun setupMovieInterestRatingSection(movieProfile: MovieProfile) {
        movie_interest_rating_bar.progress = movieProfile.movieInterestRating
        movie_interest_rating_bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int,
                                           fromUser: Boolean) {
                seekbar_text.text = "I rate my interest in Cinema : " + movie_interest_rating_bar.progress.toString() + "/10"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
        seekbar_text.text = "I rate my interest in Cinema: " + movie_interest_rating_bar.progress.toString() + "/10"
    }

    private fun setUserMovieProfileDetails(userId : String) {

        updateSelectedLists()

        // create request object
        val requestObject = JSONObject()
        requestObject.put("movie_interest_rating", movie_interest_rating_bar.progress)
        requestObject.put("genres", getSelectedList(genreList))
        requestObject.put("languages", getSelectedList(languageList))
        requestObject.put("years", getSelectedList(yearList))
        requestObject.put("years", getSelectedList(yearList))
        if (streaming_pref_radio.isChecked) {
            requestObject.put("streaming_platforms", getJSONArrayFromList(streamingPlatformList))
        } else {
            requestObject.put("streaming_platforms", JSONArray())
            streamingPlatformList = mutableListOf()
        }

        if (surprise_reco_check.isChecked) {
            requestObject.put("send_surprise_reco", true)
        } else {
            requestObject.put("send_surprise_reco", false)
        }

        var user = intent.extras?.getSerializable("user") as User
        user.movieProfile.genreList = genreList
        user.movieProfile.languageList = languageList
        user.movieProfile.yearList = yearList
        user.movieProfile.streamingPlatformList = streamingPlatformList
        user.movieProfile.movieInterestRating = movie_interest_rating_bar.progress
        user.movieProfile.sendSurpriseRecommendation = surprise_reco_check.isChecked
        intent.putExtra("user", user)

        ServiceVolley().post("/user/movie/profile?userid=" + userId+ "&access_token=" + AccessToken.getCurrentAccessToken().token, requestObject) { response ->
            Log.d(this.localClassName, response.toString())
            // show toast - both success and failure
            onBackPressed()
            Toast.makeText(this, "Movie Profile Updated", Toast.LENGTH_SHORT).show()
        }
    }
}
