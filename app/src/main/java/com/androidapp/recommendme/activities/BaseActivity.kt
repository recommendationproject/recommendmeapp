package com.androidapp.recommendme.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.recommendme.R
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.helpers.ResponseParser
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.activity_home_page.*
import kotlinx.android.synthetic.main.activity_home_page.login_progress_bar
import org.json.JSONObject
import java.lang.Exception
import java.time.Instant

class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        val sharedPreferences: SharedPreferences = this.getSharedPreferences("user_prefs", Context.MODE_PRIVATE)
        var isLoggedIn = false
        var accessTokenString: String = ""
        var facebookUserId: String = ""
        var userEmail: String = ""

        if (sharedPreferences.getString("access_token", null) != null && sharedPreferences.getLong("access_token_expiry", 1) > System.currentTimeMillis()) {
            accessTokenString = sharedPreferences.getString("access_token", null).toString()
            facebookUserId = sharedPreferences.getString("fb_user_id", null).toString()
            userEmail = sharedPreferences.getString("user_email", null).toString()
            isLoggedIn = accessTokenString != null && facebookUserId != null && userEmail != null
        }

//        val accessToken = AccessToken.getCurrentAccessToken()
//        val isLoggedIn = accessToken != null && !accessToken.isExpired


        // Start recommendations activity when user logged in
        if (isLoggedIn) {
            if (isOnline(this.applicationContext)) {
                //executeGraphRequestAndLogin(accessToken)
                var jsonObject = JSONObject()
                jsonObject.put("id", facebookUserId)
                jsonObject.put("email", userEmail)

//                LoginManager.getInstance().logInWithReadPermissions(this, listOf(
//                    "public_profile",
//                    "email"
//                ))

                fbUserLogin(jsonObject, accessTokenString = accessTokenString)
            } else {
                base_activity_no_internet.visibility = View.VISIBLE
            }
        } else {
            startActivity(Intent(this, HomePageActivity::class.java))
            this.finish()
        }
    }

    private fun executeGraphRequestAndLogin(accessToken: AccessToken?) {
        val request = GraphRequest.newMeRequest(accessToken) { `object`, response ->
            try {
                fbUserLogin(response.jsonObject, AccessToken.getCurrentAccessToken().token)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val parameters = Bundle()
//        parameters.putString(
//            "fields",
//            "email,id, first_name, last_name, gender, birthday, verified, location, friends"
//        )
        parameters.putString(
            "fields",
            "email,id, first_name, last_name, gender, friends"
        )

        request.parameters = parameters
        request.executeAsync()
    }

    private fun fbUserLogin(params: JSONObject, accessTokenString: String) {
        Log.d(this.localClassName, params.toString())

        login_progress_bar.visibility = View.VISIBLE

        ServiceVolley()
            .post("/user/login?access_token=" + accessTokenString, params) { response ->
            Log.d(this.localClassName, response.toString())

            login_progress_bar.visibility = View.GONE

            val isNewUser = response?.getBoolean("is_new_user")
            val userId = response?.getString("userid")

            if (userId != null) {

                retrieveFirbaseToken(userid = userId)

                var userObj = ResponseParser()
                    .parseUserLoginResponse(response, isNewUser)
                if (isNewUser == true) {
                    var intent = Intent(this, MovieProfileActivity::class.java)
                    intent.putExtra("user", userObj)
                    startActivity(intent)
                } else {
                    var intent = Intent(this, RecommendationsActivity::class.java)
                    intent.putExtra("user", userObj)
                    intent.putExtra("userId", userId)
                    startActivity(intent)
                    this.finish()
                }
            } else {
                //TODO start a failure activity
            }

        }
    }

    private fun retrieveFirbaseToken(userid: String) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(this.localClassName, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log
                val msg = getString(R.string.msg_token_fmt, token)
                Log.d("FIREBASE_TOKEN", msg)

                if (token != null) {
                    sendRegistrationToServer(userid = userid, token = token)
                }
            })
    }

    private fun sendRegistrationToServer(userid: String, token: String) {
        //TODO send firebase token along with login request - avoid multiple calls
        ServiceVolley().put(
            "/user/firebase_token?userid=" + userid + "&firebase_token=" + token,
            JSONObject()
        ) { response ->
            Log.d(this.localClassName, response.toString())
        }
    }

    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val n = cm.activeNetwork
            if (n != null) {
                val nc = cm.getNetworkCapabilities(n)
                //It will check for both wifi and cellular network
                return nc!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
            }
            return false
        } else {
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }
    }

}
