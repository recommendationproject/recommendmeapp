package com.androidapp.recommendme.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.androidapp.recommendme.OnDemandMovieOption
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.User
import com.example.recommendme.R
import com.facebook.AccessToken
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import kotlinx.android.synthetic.main.activity_on_demand_recommendations.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject


class OnDemandRecommendationsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_demand_recommendations)

        //setup toolbar
        toolbar_title.text = "On Demand Movies"
        toolbar_left_button.setImageResource(R.drawable.arrow_back)
        toolbar_left_button.setOnClickListener {
            onBackPressed()
        }

        // Get intent details
        val extras = intent.extras
        var user = extras?.getSerializable("user") as User
        val userId = user.userId
        val onDemandMovieMonthlyRemainingQuota = user.remainingOnDemandMoviesCount
        val onDemandMovieOptions = user.onDemandMovieOptions

        if (onDemandMovieMonthlyRemainingQuota <= 0) {
            on_demand_movie_request_generate_layout.visibility = View.GONE
            on_demand_movie_request_quota_over.visibility = View.VISIBLE
        }

        val tf1 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Demi.otf")
        val tf2 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Regular.otf")

        on_demand_reco_quota_title.typeface = tf1
        on_demand_reco_quota_value.typeface = tf1
        on_demand_reco_quota_footer.typeface = tf2

        on_demand_reco_quota_value.text = onDemandMovieMonthlyRemainingQuota.toString()

        for (movieOption in onDemandMovieOptions) {
            val movieOptionChip = Chip(this)
            movieOptionChip.setChipDrawable(ChipDrawable.createFromResource(this, R.xml.ondemand_movie_option_chip))
            movieOptionChip.isCheckedIconVisible = false
            movieOptionChip.isCloseIconVisible = false
            movieOptionChip.text = movieOption.text

            val scale = this.resources.displayMetrics.density
            movieOptionChip.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                (40 * scale + 0.5f).toInt()
            )

            event_movie_chip_group.addView(movieOptionChip)
        }

        generate_on_demand_recos.setOnClickListener {
            generate_on_demand_recos.isClickable = false
            if (event_movie_chip_group.checkedChipId == -1) {
                Toast.makeText(this, "Select an option", Toast.LENGTH_SHORT).show()
                generate_on_demand_recos.isClickable = true
            } else {
                var selectedText =  event_movie_chip_group.findViewById<Chip>(event_movie_chip_group.checkedChipId).text.toString()
                var selectedTag = ""
                for (movieOption in onDemandMovieOptions) {
                    if (movieOption.text == selectedText) {
                        selectedTag = movieOption.tag
                        break
                    }
                }

                if (selectedTag != "") {
                    on_demand_movie_request_generate_layout.alpha = 0.2.toFloat()
                    on_demand_movie_request_submitting_progress.visibility = View.VISIBLE

                    var requestObject = JSONObject()
                    ServiceVolley().post("/user/ondemand/recommendations?access_token=" + AccessToken.getCurrentAccessToken().token + "&userid=" + userId + "&event_type=" + selectedTag,
                        requestObject) { response ->
                        if (response != null) {
                            // Show another frame
                            on_demand_movie_request_submitting_progress.visibility = View.GONE
                            on_demand_movie_request_generate_layout.visibility = View.GONE
                            on_demand_movie_request_submitted_layout.visibility = View.VISIBLE

                            // Decrease Remaining quota count
                            var returnIntent = Intent()
                            user.remainingOnDemandMoviesCount = user.remainingOnDemandMoviesCount - 1
                            returnIntent.putExtra("user", user)
                            setResult(Activity.RESULT_OK, returnIntent)
                        } else {
                            Toast.makeText(this, "Failed to request movies", Toast.LENGTH_LONG).show()
                        }
                    }
                } else {
                    Toast.makeText(this, "Failed to request movies", Toast.LENGTH_LONG).show()
                    generate_on_demand_recos.isClickable = true
                }
            }
        }
    }
}
