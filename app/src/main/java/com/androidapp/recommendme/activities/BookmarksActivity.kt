package com.androidapp.recommendme.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.androidapp.recommendme.Recommendation
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.adapters.BookmarksAdapter
import com.example.recommendme.R
import com.facebook.AccessToken
import kotlinx.android.synthetic.main.activity_bookmarks.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray

class BookmarksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookmarks)

        //setup toolbar
        toolbar_title.text = "Bookmarks"
        toolbar_left_button.setImageResource(R.drawable.arrow_back)
        toolbar_left_button.setOnClickListener {
            onBackPressed()
        }

        getBookmarks()
    }

    private fun getBookmarks() {
        val extras = intent.extras
        val userId = extras?.getString("userId")

        var bookmarks: MutableList<Recommendation> = mutableListOf()
        ServiceVolley()
            .get("/user/recommendations?userid=" + userId + "&access_token=" + AccessToken.getCurrentAccessToken().token + "&filter=bookmarks") { response ->
                Log.d("BOOKMARKS_RESPONSE", response.toString())
                val responseRecommendations = response?.get("recommendations")

                if (responseRecommendations != null && responseRecommendations is JSONArray && responseRecommendations.length() > 0) {

                    bookmarks_progress_bar.visibility = View.GONE
                    bookmarks_list.visibility = View.VISIBLE

                    for (i in 0 until (responseRecommendations.length())) {
                        val recommendation = responseRecommendations.getJSONObject(i)
                        val category = recommendation.getString("category")
                        val title = recommendation.getString("title")
                        val shortDescription = recommendation.getString("short_description")
                        val recommendationId = recommendation.getString("id")
                        val isBookmarked = recommendation.getBoolean("is_bookmarked")

                        bookmarks.add(
                            Recommendation(
                                category = category,
                                id = recommendationId,
                                isBookmarked = isBookmarked,
                                title = title,
                                description = shortDescription,
                                details_json = recommendation
                            )
                        )

                    }

                    val bookmarksAdapter = BookmarksAdapter(this, bookmarks)
                    bookmarks_list.adapter = bookmarksAdapter
                } else {
                    //TODO Hide bookmarks loading screen
                    //TODO Show empty list message
                }
            }
    }
}
