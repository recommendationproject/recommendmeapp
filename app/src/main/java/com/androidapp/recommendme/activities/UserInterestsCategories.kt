package com.androidapp.recommendme.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.androidapp.recommendme.Categories
import com.example.recommendme.R
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.User
import kotlinx.android.synthetic.main.activity_user_interests_categories.*
import org.json.JSONArray
import org.json.JSONObject

class UserInterestsCategories : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_interests_categories)
        interests_category_continue_button.isEnabled = false

        var isMovieSelected = false
        var isRestaurantSelected = false
        var isVideoSelected = false

        var user = intent.extras?.getSerializable("user") as User

        movie_category.setOnClickListener {
            if (!isMovieSelected) {
                movie_category.alpha = 0.2.toFloat()
                isMovieSelected = true
                setupContinueButton(isMovieSelected or isRestaurantSelected or isVideoSelected)
            } else {
                movie_category.alpha = 1.0.toFloat()
                isMovieSelected = false
                setupContinueButton(isMovieSelected or isRestaurantSelected or isVideoSelected)
            }
        }

        restaurants_category.setOnClickListener {
            if (!isRestaurantSelected) {
                restaurants_category.alpha = 0.2.toFloat()
                isRestaurantSelected = true
                setupContinueButton(isMovieSelected or isRestaurantSelected or isVideoSelected)
            } else {
                restaurants_category.alpha = 1.0.toFloat()
                isRestaurantSelected = false
                setupContinueButton(isMovieSelected or isRestaurantSelected or isVideoSelected)
            }
        }

        videos_category.setOnClickListener {
            if (!isVideoSelected) {
                videos_category.alpha = 0.2.toFloat()
                isVideoSelected = true
                setupContinueButton(isMovieSelected or isRestaurantSelected or isVideoSelected)
            } else {
                videos_category.alpha = 1.0.toFloat()
                isVideoSelected = false
                setupContinueButton(isMovieSelected or isRestaurantSelected or isVideoSelected)
            }
        }


        interests_category_continue_button.setOnClickListener {

            var categories = mutableListOf<String>()
            if (isMovieSelected)
                categories.add("movies")
            if (isRestaurantSelected)
                categories.add("restaurants")
            if (isVideoSelected)
                categories.add("videos")

            val userId = user.userId
            var params = JSONObject()
            val path = "/user/profile?userId=" + userId + "&action=update_interest_categories&categories=" + categories.joinToString(separator = ",")

            ServiceVolley().put(path, params) { response ->
                Log.d(this.localClassName, response.toString())

                if (response?.get("categories") != null && response?.get("categories") is JSONArray) {
                    var intent = Intent(this, UserInterestsActivity::class.java)
                    var categories = mutableListOf<String>()
                    var responseCategories = response?.get("categories") as JSONArray
                    for (i in 0 until responseCategories.length())
                        categories.add(responseCategories.getString(i))

                    intent.putExtra("categories",
                        Categories(categories = categories)
                    )

                    user.interestsCategories = categories

                    intent.putExtra("userId", userId)
                    intent.putExtra("user", user)
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "Something went wrong !! ", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun setupContinueButton(isCategorySelected:Boolean) {
        if (isCategorySelected) {
            interests_category_continue_button.isEnabled = true
            interests_category_continue_button.setImageResource(R.drawable.continue_yellow)
        } else {
            interests_category_continue_button.isEnabled = false
            interests_category_continue_button.setImageResource(R.drawable.continue_grey)
        }
    }
}
