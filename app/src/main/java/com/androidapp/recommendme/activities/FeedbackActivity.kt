package com.androidapp.recommendme.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.recommendme.R
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.User
import com.facebook.AccessToken
import kotlinx.android.synthetic.main.activity_feedback.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject
import android.content.Context
import android.view.inputmethod.InputMethodManager


class FeedbackActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)

        //setup toolbar
        toolbar_title.text = "Submit feedback"
        toolbar_left_button.setImageResource(R.drawable.arrow_back)
        toolbar_left_button.setOnClickListener {
            onBackPressed()
        }

        submit_feedback.setOnClickListener {
            if (feedback_text_input.text != null && feedback_text_input.text.isNotEmpty()) {

                // Hide keyboard
                val view = this.currentFocus
                if (view != null) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }

                val extras = intent.extras
                val user = extras?.getSerializable("user") as User

                var requestObject = JSONObject()
                requestObject.put("feedback_text", feedback_text_input.text)

                ServiceVolley().post("/feedback?access_token=" + AccessToken.getCurrentAccessToken().token + "&userid=" + user.userId , requestObject) { response ->
                    Log.d("SUBMIT_FEEDBACK", response.toString())
                    // Show thank you page
                    submit_feedback_layout.visibility = View.GONE
                    feedback_thanks_layout.visibility = View.VISIBLE
                }
            }
        }
    }
}
