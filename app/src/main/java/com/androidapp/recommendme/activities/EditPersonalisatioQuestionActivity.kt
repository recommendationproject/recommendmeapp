package com.androidapp.recommendme.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.User
import com.androidapp.recommendme.UserInterestAnswerOption
import com.androidapp.recommendme.UserInterestQuestion
import com.example.recommendme.*
import com.androidapp.recommendme.adapters.AnswerOptionAdapter
import kotlinx.android.synthetic.main.activity_edit_personalisation_question.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONObject

class EditPersonalisationQuestionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_personalisation_question)

        //setup toolbar
        toolbar_left_button.setImageResource(R.drawable.arrow_back)
        toolbar_left_button.setOnClickListener {
            onBackPressed()
        }

        // Get data from intent
        val extras = intent.extras
        val question = extras?.getSerializable("question") as UserInterestQuestion
        val user = extras?.getSerializable("user") as User

        val answerOptions = question.options
        val answerOptionsAdapter =
            AnswerOptionAdapter(this, answerOptions)

        var selectedAnswer = UserInterestAnswerOption(
            text = "",
            property = "",
            isSelected = false
        )
        for (i in 0 until answerOptions.size) {
            var answerOption = answerOptions.get(i)
            if (answerOption.isSelected) {
                selectedAnswer = answerOption
                answerOptionsAdapter.setSelectedIndex(i)
                break
            }
        }

        answer_options.setOnItemClickListener { _, _, position, _ ->
            answerOptionsAdapter.setSelectedIndex(position)
            selectedAnswer = answerOptions.get(position)
        }

        answer_options.adapter = answerOptionsAdapter

        interests_coninue_button.setOnClickListener {
            var answers = JSONArray()
            answers.put(selectedAnswer.property)

            var params = JSONObject()
            params.put("question_property", question.questionProperty)
            params.put("category", question.category)
            params.put("answers", answers)

            ServiceVolley()
                .put("/user/personalisation?userid=" + user.userId, params) { response ->
                Log.d("USER_PERSONALISATION", response.toString())

                // Update user object
                for (answerOption in answerOptions) {
                    answerOption.isSelected = false
                    if (answerOption.property == selectedAnswer.property) {
                        answerOption.isSelected = true
                    }
                }

                for (answeredQuestion in user.answeredPersonalisationQuestions) {
                    if (answeredQuestion.questionProperty == question.questionProperty && answeredQuestion.category == question.category) {
                        answeredQuestion.options = answerOptions
                    }
                }

                var returnIntent = Intent()
                returnIntent.putExtra("user", user)
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
            }

        }
    }
}
