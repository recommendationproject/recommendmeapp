package com.androidapp.recommendme.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.recommendme.R
import com.androidapp.recommendme.ServiceVolley

import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class SignupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        //setup toolbar
        toolbar_title.text = "Profile"

        val extras = intent.extras
        if (extras != null) {
            name_text_input.setText(extras.getString("first_name"))
            email_text_input.setText(extras.getString("email"))

            var gender = extras.getString("gender")
            if (gender == "male") {
                radio_male.isChecked = true
            } else if (gender == "female") {
                radio_female.isChecked = true
            }

            var location = extras.getString("location")
            if (location != null) {
                city_text_input.setText(location)
            }

            var dob = extras.getString("birthday")
            if (dob != null) {
                dob_text_input.setText(dob)
            }
        }

        register_button.setOnClickListener(View.OnClickListener { v ->
            // create request object
            val requestObject = JSONObject()
            requestObject.put("first_name", name_text_input.text)
            requestObject.put("email", email_text_input.text)

            if (radio_male.isChecked)
                requestObject.put("gender", "male")
            else if (radio_female.isChecked)
                requestObject.put("gender", "female")

            setUserProfileDetails(requestObject)

            //start another activity
            val intent = Intent(this, RecommendationsActivity::class.java)
            startActivity(intent)
        })
    }

    fun setUserProfileDetails(requestObject : JSONObject) {
        ServiceVolley().post("/user/profile", requestObject) { response ->
            Log.d(this.localClassName, response.toString())
        }
    }
}
