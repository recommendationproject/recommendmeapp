package com.androidapp.recommendme.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.recommendme.R
import kotlinx.android.synthetic.main.activity_privacy_policy.*
import kotlinx.android.synthetic.main.toolbar.*

class PrivacyPolicyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)

        // setup toolbar
        toolbar_left_button.setImageResource(R.drawable.arrow_back)
        toolbar_left_button.setOnClickListener {
            onBackPressed()
        }

        privacyPolicyWebView.loadUrl("file:///android_asset/privacy_policy.html")
    }
}
