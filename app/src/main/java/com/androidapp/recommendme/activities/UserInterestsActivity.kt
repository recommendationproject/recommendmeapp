package com.androidapp.recommendme.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.androidapp.recommendme.fragments.InterestsQuestionFragment
import com.example.recommendme.R

class UserInterestsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_interests)

        val fm = supportFragmentManager
        var fragment = fm.findFragmentById(R.id.interests_questions_fragment_container)

        // ensures fragments already created will not be created
        if (fragment == null) {
            fragment = InterestsQuestionFragment()
            // create and commit a fragment transaction
            fm.beginTransaction()
                .add(R.id.interests_questions_fragment_container, fragment)
                .commit()
        }
    }
}
