package com.androidapp.recommendme.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.recommendme.R
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.User
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.active_recommendation_card.view.*
import kotlinx.android.synthetic.main.activity_user_profile.*
import kotlinx.android.synthetic.main.interests_category_item.view.*
import kotlinx.android.synthetic.main.profile_categories_item.view.*
import kotlinx.android.synthetic.main.profile_item.view.*
import kotlinx.android.synthetic.main.profile_item.view.profile_item_text
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.user_interests_categories.view.*
import org.json.JSONArray
import org.json.JSONObject

class UserProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        //setup toolbar
        toolbar_title.text = "Profile"
        toolbar_left_button.setImageResource(R.drawable.arrow_back)
        toolbar_left_button.setOnClickListener {
            onBackPressed()
        }

        var extras = intent.extras
        var user = extras?.getSerializable("user") as User

        val tf2 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Bold.otf")
        profile_name.typeface = tf2
        profile_name.text = user.firstName + " " + user.lastName
        Glide.with(this)
            .load(user.profilePictureUrl)
            .into(profile_pic)
            .waitForLayout()

        val inflater = LayoutInflater.from(this)
        val scale = this.resources.displayMetrics.density
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            (48 * scale + 0.5f).toInt()
        )
        layoutParams.setMargins(0, (8 * scale + 0.5f).toInt(), 0, 0)

        // Supporting only for movie for now
        //inflateRecommendationCategories(inflater)
        inflatePersonalisationSettings(inflater, layoutParams)
        inflateOnDemandRecoOption(inflater, layoutParams)
        inflateBookmarks(inflater, layoutParams)
        //inflateCityOption(inflater, layoutParams)
        inflateHelpOption(inflater, layoutParams)
        inflateSignOutOption(inflater, layoutParams)
    }

    override fun onBackPressed() {
        var extras = intent.extras
        var user = extras?.getSerializable("user") as User

        var returnIntent = Intent()
        returnIntent.putExtra("user", user)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            val user = data.getSerializableExtra("user") as User
            intent.putExtra("user", user)
            profile_options.invalidate()
        }
    }

    private fun inflateRecommendationCategories(inflater: LayoutInflater) {
        val profileCategoriesItem = inflater.inflate(R.layout.profile_categories_item, null, false)
        val categoriesLayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val tf1 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Regular.otf")
        profileCategoriesItem.profile_item_text.setTypeface(tf1)

        profileCategoriesItem.profile_categories.movies_category.category_text.text = "Movies"
        profileCategoriesItem.profile_categories.restaurants_category.category_text.text =
            "Restaurants"
        profileCategoriesItem.profile_categories.videos_category.category_text.text = "Videos"

        profileCategoriesItem.profile_categories.movies_category.category_image.setImageResource(R.drawable.movies_category_image)
        profileCategoriesItem.profile_categories.restaurants_category.category_image.setImageResource(
            R.drawable.restaurants_category_image
        )
        profileCategoriesItem.profile_categories.videos_category.category_image.setImageResource(R.drawable.memes_category_image)

        var user = getUserFromIntent()
        var interests = user.interestsCategories.toMutableList()

        profileCategoriesItem.profile_categories.movies_category.category_card.setOnClickListener {
            if ("movies" in interests) {
                interests.remove("movies")
                profileCategoriesItem.profile_categories.movies_category.category_image.alpha =
                    1.0.toFloat()
            } else {
                interests.add("movies")
                profileCategoriesItem.profile_categories.movies_category.category_image.alpha =
                    0.2.toFloat()
            }
            if (interests.size > 0) {
                profileCategoriesItem.save_profile_categories.visibility = View.VISIBLE
            } else {
                profileCategoriesItem.save_profile_categories.visibility = View.GONE
            }
        }

        profileCategoriesItem.profile_categories.restaurants_category.category_card.setOnClickListener {
            if ("restaurants" in interests) {
                interests.remove("restaurants")
                profileCategoriesItem.profile_categories.restaurants_category.category_image.alpha =
                    1.0.toFloat()
            } else {
                interests.add("restaurants")
                profileCategoriesItem.profile_categories.restaurants_category.category_image.alpha =
                    0.2.toFloat()
            }
            if (interests.size > 0) {
                profileCategoriesItem.save_profile_categories.visibility = View.VISIBLE
            } else {
                profileCategoriesItem.save_profile_categories.visibility = View.GONE
            }
        }

        profileCategoriesItem.profile_categories.videos_category.category_card.setOnClickListener {
            if ("videos" in interests) {
                interests.remove("videos")
                profileCategoriesItem.profile_categories.videos_category.category_image.alpha =
                    1.0.toFloat()
            } else {
                interests.add("videos")
                profileCategoriesItem.profile_categories.videos_category.category_image.alpha =
                    0.2.toFloat()
            }
            if (interests.size > 0) {
                profileCategoriesItem.save_profile_categories.visibility = View.VISIBLE
            } else {
                profileCategoriesItem.save_profile_categories.visibility = View.GONE
            }
        }

        profileCategoriesItem.save_profile_categories.setOnClickListener {
            val userId = user.userId
            var params = JSONObject()
            val path = "/user/profile?userId=" + userId + "&action=update_interest_categories&categories=" + interests.joinToString(separator = ",")

            ServiceVolley().put(path, params) { response ->
                Log.d(this.localClassName, response.toString())

                if (response?.get("categories") != null && response?.get("categories") is JSONArray) {
                    var categories = mutableListOf<String>()
                    var responseCategories = response?.get("categories") as JSONArray
                    for (i in 0 until responseCategories.length())
                        categories.add(responseCategories.getString(i))

                    user.interestsCategories = categories
                    intent.putExtra("user", user)
                    profileCategoriesItem.save_profile_categories.visibility = View.GONE
                    Toast.makeText(this, "Updated your interests", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, "Something went wrong !! ", Toast.LENGTH_LONG).show()
                }
            }
        }

        for (interest in interests) {
            when (interest) {
                "movies" -> {
                    profileCategoriesItem.profile_categories.movies_category.category_image.alpha =
                        0.2.toFloat()
                }
                "restaurants" -> {
                    profileCategoriesItem.profile_categories.restaurants_category.category_image.alpha =
                        0.2.toFloat()
                }
                "videos" -> {
                    profileCategoriesItem.profile_categories.videos_category.category_image.alpha =
                        0.2.toFloat()
                }
            }
        }

        profile_options.addView(profileCategoriesItem, categoriesLayoutParams)

    }

    private fun inflatePersonalisationSettings(
        inflater: LayoutInflater,
        layoutParams: LinearLayout.LayoutParams
    ) {
        val personalisationSettings = inflater.inflate(R.layout.profile_item, null, false)

        val tf1 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Regular.otf")
        personalisationSettings.profile_item_text.setTypeface(tf1)
        personalisationSettings.profile_item_text.text = "Personalisation Settings"

        personalisationSettings.setOnClickListener {
            var intent = Intent(this, MovieProfileActivity::class.java)
            intent.putExtra("user", getUserFromIntent())
            intent.putExtra("viaProfile", "true")
            startActivityForResult(intent, 1)
        }

        profile_options.addView(personalisationSettings, layoutParams)
    }

    private fun inflateBookmarks(
        inflater: LayoutInflater,
        layoutParams: LinearLayout.LayoutParams
    ) {
        val bookmarks = inflater.inflate(R.layout.profile_item, null, false)

        val tf1 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Regular.otf")
        bookmarks.profile_item_text.setTypeface(tf1)
        bookmarks.profile_item_text.text = "Bookmarks"

        bookmarks.setOnClickListener {
            val extras = intent.extras
            val user = extras?.getSerializable("user") as User

            //var intent = Intent(this, BookmarksActivity::class.java)
            var intent = Intent(this, RecommendationsActivity::class.java)
            intent.putExtra("recommendations_type", "bookmarks")
            intent.putExtra("user", user)
            intent.putExtra("userId", user.userId)
            startActivity(intent)
        }

        profile_options.addView(bookmarks, layoutParams)
    }

    private fun inflateCityOption(
        inflater: LayoutInflater,
        layoutParams: LinearLayout.LayoutParams
    ) {
        val city = inflater.inflate(R.layout.profile_item, null, false)

        val tf1 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Regular.otf")
        city.profile_item_text.setTypeface(tf1)
        city.profile_item_text.text = "From Bangalore, Karnataka"

        city.setOnClickListener {
            //Spinner.
        }

        profile_options.addView(city, layoutParams)
    }

    private fun inflateHelpOption(
        inflater: LayoutInflater,
        layoutParams: LinearLayout.LayoutParams
    ) {
        val help = inflater.inflate(R.layout.profile_item, null, false)

        val tf1 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Regular.otf")
        help.profile_item_text.setTypeface(tf1)
        help.profile_item_text.text = "Feedback"
        profile_options.addView(help, layoutParams)

        help.setOnClickListener {
            val extras = intent.extras
            val user = extras?.getSerializable("user") as User
            var intent = Intent(this, FeedbackActivity::class.java)
            intent.putExtra("user", user)
            startActivity(intent)
        }
    }

    private fun inflateOnDemandRecoOption(
        inflater: LayoutInflater,
        layoutParams: LinearLayout.LayoutParams
    ) {
        val help = inflater.inflate(R.layout.profile_item, null, false)

        val tf1 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Regular.otf")
        help.profile_item_text.setTypeface(tf1)
        help.profile_item_text.text = "On Demand Movies"
        profile_options.addView(help, layoutParams)

        help.setOnClickListener {
            val extras = intent.extras
            val user = extras?.getSerializable("user") as User
            var intent = Intent(this, OnDemandRecommendationsActivity::class.java)
            intent.putExtra("user", user)
            startActivityForResult(intent, 1)
        }
    }

    private fun inflateSignOutOption(
        inflater: LayoutInflater,
        layoutParams: LinearLayout.LayoutParams
    ) {
        val signOut = inflater.inflate(R.layout.profile_item, null, false)

        val tf1 = Typeface.createFromAsset(assets, "AvenirNextLTPro-Regular.otf")
        signOut.profile_item_text.setTypeface(tf1)
        signOut.profile_item_text.text = "Sign Out"
        signOut.profile_item_right_button.visibility = View.INVISIBLE

        var user: User = getUserFromIntent()
        signOut.setOnClickListener {
            ServiceVolley().post("/user/logout?userid=" + user.userId + "&access_token=" + AccessToken.getCurrentAccessToken().token, JSONObject()) { response ->
                Log.d(this.localClassName, response.toString())
                // show toast - both success and failure
            }

            LoginManager.getInstance().logOut()

            var sharedPreferences: SharedPreferences = this.getSharedPreferences("user_prefs", Context.MODE_PRIVATE)
            sharedPreferences.edit().clear().commit()

            startActivity(Intent(this, HomePageActivity::class.java))
            this.finish()
        }

        profile_options.addView(signOut, layoutParams)
    }

    private fun getUserFromIntent(): User {
        var extras = intent.extras
        var user = extras?.getSerializable("user") as User
        return user
    }
}
