package com.androidapp.recommendme.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.SpannedString
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import com.androidapp.recommendme.ServiceVolley
import com.example.recommendme.*
import com.androidapp.recommendme.helpers.ResponseParser
import com.facebook.*
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.activity_home_page.*
import org.json.JSONObject
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import java.lang.Exception

class HomePageActivity : AppCompatActivity() {

    private var fbCallbackManager = CallbackManager.Factory.create()
    private val TAG = SignupActivity::class.java.canonicalName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)

        LoginManager.getInstance()
            .registerCallback(fbCallbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    executeGraphRequestAndLogin(result?.accessToken)
                }

                override fun onCancel() {
                    Log.d(TAG, "Facebook login canceled")
                    startActivity(Intent(this@HomePageActivity, HomePageActivity::class.java))
                }

                override fun onError(error: FacebookException?) {
                    Log.d(TAG, error?.message)
                }
            })

        fb_login_custom_button.setOnClickListener {
            val permissionsList = listOf(
                "public_profile",
                "email",
                "user_friends"
            )

            LoginManager.getInstance().logInWithReadPermissions(this, permissionsList)
            //home_page.visibility = View.GONE
            home_page.alpha = 0.2.toFloat()
            home_page_background.alpha = 0.2.toFloat()
        }

        handlePrivacyPolicyText()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        fbCallbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun executeGraphRequestAndLogin(accessToken: AccessToken?) {
        val request = GraphRequest.newMeRequest(accessToken) { `object`, response ->
            try {
                fbUserLogin(response.jsonObject)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val parameters = Bundle()
        parameters.putString(
            "fields",
            "email,id, first_name, last_name"
        )

        request.parameters = parameters
        request.executeAsync()
    }

    private fun fbUserLogin(params: JSONObject) {
        Log.d(this.localClassName, params.toString())
        val accessToken = AccessToken.getCurrentAccessToken()
        val accessTokenString = accessToken.token

        login_progress_bar.visibility = View.VISIBLE

        ServiceVolley()
            .post("/user/login?access_token=" + accessTokenString, params) { response ->
            Log.d(this.localClassName, response.toString())

            login_progress_bar.visibility = View.GONE

            val isNewUser = response?.getBoolean("is_new_user")
            val userId = response?.getString("userid")
            if (userId != null) {

                retrieveFirbaseToken(userid = userId)

                // Save user id and accessToken in shared prefs
                saveUserDetailsInSharedPrefs(accessToken, userId, params.getString("id"), params.getString("email"))

                var userObj = ResponseParser()
                    .parseUserLoginResponse(response, isNewUser)
                if (isNewUser == true) {
                    var intent = Intent(this, MovieProfileActivity::class.java)
                    intent.putExtra("user", userObj)
                    startActivity(intent)
                    this.finish()
                } else {
                    var intent = Intent(this, RecommendationsActivity::class.java)
                    intent.putExtra("user", userObj)
                    intent.putExtra("userId", userId)
                    startActivity(intent)
                    this.finish()
                }
            } else {
                //TODO start a failure activity
            }
        }
    }

    private fun retrieveFirbaseToken(userid: String) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(this.localClassName, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log
                val msg = getString(R.string.msg_token_fmt, token)
                Log.d("FIREBASE_TOKEN", msg)

                if (token != null) {
                    sendRegistrationToServer(userid = userid, token = token)
                }
            })
    }

    private fun sendRegistrationToServer(userid: String, token: String) {
        ServiceVolley().put(
            "/user/firebase_token?userid=" + userid + "&firebase_token=" + token,
            JSONObject()
        ) { response ->
            Log.d(this.localClassName, response.toString())
        }
    }

    private fun handlePrivacyPolicyText() {
        val termAndConditionsText =
            getText(R.string.home_page_terms_and_conditions) as SpannedString
        val spannableString = SpannableString(termAndConditionsText)

        val clickableSpanTnC = object : ClickableSpan() {
            override fun onClick(p0: View) {
                startActivity(Intent(this@HomePageActivity, PrivacyPolicyActivity::class.java))
                //Snackbar.make(window.decorView.rootView, "URL is clicked", Snackbar.LENGTH_SHORT).show()
            }
        }

        val clickableSpanPrivacyPolicy = object : ClickableSpan() {
            override fun onClick(p0: View) {
                startActivity(Intent(this@HomePageActivity, PrivacyPolicyActivity::class.java))
                //Snackbar.make(window.decorView.rootView, "URL is clicked", Snackbar.LENGTH_SHORT).show()
            }
        }

        spannableString.setSpan(clickableSpanTnC, 44, 60, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(
            clickableSpanPrivacyPolicy,
            65,
            79,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

//        termsAndConditionsTextView.apply {
//            text = spannableString
//            movementMethod = LinkMovementMethod.getInstance()
//        }
    }

    private fun saveUserDetailsInSharedPrefs(accessToken: AccessToken, userId: String, facebookId: String, email: String) {
        val sharedPreferences: SharedPreferences = this.getSharedPreferences("user_prefs", Context.MODE_PRIVATE)
        val editor:SharedPreferences.Editor =  sharedPreferences.edit()
        editor.putString("access_token",accessToken.token)
        editor.putLong("access_token_expiry",accessToken.expires.time)
        editor.putString("user_id",userId)
        editor.putString("fb_user_id",facebookId)
        editor.putString("user_email",email)
        editor.apply()
        editor.commit()
    }
}
