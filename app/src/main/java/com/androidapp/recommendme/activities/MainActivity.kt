package com.androidapp.recommendme.activities

import android.os.Bundle
import com.example.recommendme.R
import com.google.android.youtube.player.*


class MainActivity : YouTubeBaseActivity() {

   // private var callbackManager = CallbackManager.Factory.create()

   /* companion object {
        val VIDEO_ID: String = "YqggV_wGhM0";
        val YOUTUBE_API_KEY: String = "AIzaSyAGYLzqvpEt3RECmwamycY217516f6frug"
    }

    lateinit var youtubePlayerInit: YouTubePlayer.OnInitializedListener*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*val youtubePlayerView = YouTubePlayerView(this)
        youtubePlayerView.visibility = View.INVISIBLE

        youtubeThumbnailView.initialize(YOUTUBE_API_KEY, object : YouTubeThumbnailView.OnInitializedListener {
            override fun onInitializationSuccess(p0: YouTubeThumbnailView?, ytThumbnailLoader: YouTubeThumbnailLoader?) {
                ytThumbnailLoader?.setVideo(VIDEO_ID)
                ytThumbnailLoader?.setOnThumbnailLoadedListener(object : YouTubeThumbnailLoader.OnThumbnailLoadedListener {
                    override fun onThumbnailLoaded(p0: YouTubeThumbnailView?, p1: String?) {
                        ytThumbnailLoader?.release()
                    }

                    override fun onThumbnailError(p0: YouTubeThumbnailView?, p1: YouTubeThumbnailLoader.ErrorReason?) {
                    }

                })
            }

            override fun onInitializationFailure(p0: YouTubeThumbnailView?, p1: YouTubeInitializationResult?) {
                Toast.makeText(applicationContext, "Something went wrong !! ", Toast.LENGTH_SHORT).show()
            }
        })

        youtubePlayerInit = object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, youtubePlayer: YouTubePlayer?, p2: Boolean) {
                youtubePlayer?.cueVideo(VIDEO_ID)
            }

            override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
                Toast.makeText(applicationContext, "Something went wrong !! ", Toast.LENGTH_SHORT).show()
            }
        }

        btnPlay.setOnClickListener(View.OnClickListener { v ->
            youtubePlayerView.initialize(YOUTUBE_API_KEY, youtubePlayerInit)
            youtubeThumbnailView.visibility = View.INVISIBLE
            btnPlay.visibility = View.INVISIBLE
            youtubePlayerView.visibility = View.VISIBLE
        })*/



/*
        login_with_fb_button.setPermissions(listOf("email", "public_profile"))
        login_with_fb_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Log.d("FB_LOGIN_SUCCESS", result?.accessToken?.token)
            }

            override fun onCancel() {
                Log.d("FB_LOGIN", "FAILED")
            }

            override fun onError(error: FacebookException?) {
                Log.d("FB_LOGIN", "FAILED")
            }

        })
*/

        /**
         * Recommendation fragment changes
        val fm = supportFragmentManager
        var fragment = fm.findFragmentById(R.id.fragment_container)

        // ensures fragments already created will not be created
        if (fragment == null) {
            fragment = RecommendationFragment()
            // create and commit a fragment transaction
            fm.beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit()
        }
        **/
    }
/*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

 */


}
