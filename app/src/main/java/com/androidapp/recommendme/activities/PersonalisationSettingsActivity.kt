package com.androidapp.recommendme.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.recommendme.R
import com.androidapp.recommendme.User
import com.androidapp.recommendme.adapters.PersonalisationSettingsItemAdapter
import kotlinx.android.synthetic.main.activity_personalisation_settings.*
import kotlinx.android.synthetic.main.toolbar.*

class PersonalisationSettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personalisation_settings)

        //setup toolbar
        toolbar_left_button.setImageResource(R.drawable.arrow_back)
        toolbar_left_button.setOnClickListener {
            onBackPressed()
        }

        var extras = intent.extras
        var user = extras?.getSerializable("user") as User

        // Set personalisation questions adapter
        setPersonalisationQuestionsAdapter(user)
    }

    override fun onBackPressed() {
        var extras = intent.extras
        var user = extras?.getSerializable("user") as User

        var returnIntent = Intent()
        returnIntent.putExtra("user", user)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            val user = data.getSerializableExtra("user") as User
            intent.putExtra("user", user)

            setPersonalisationQuestionsAdapter(user)
            personalisation_questions.invalidateViews()
        }
    }

    private fun setPersonalisationQuestionsAdapter(user: User) {
        var questions = user.answeredPersonalisationQuestions

        val personalisationsAdapter =
            PersonalisationSettingsItemAdapter(this, questions)

        personalisation_questions.setOnItemClickListener { _, _, position, _ ->
            var intent = Intent(this, EditPersonalisationQuestionActivity::class.java)
            intent.putExtra("user", user)
            intent.putExtra("question", questions.get(position))
            startActivityForResult(intent, 1)
        }

        personalisation_questions.adapter = personalisationsAdapter
    }
}
