package com.androidapp.recommendme

import org.json.JSONObject

interface VolleyCallback {

    fun onSuccess(result: JSONObject)
}
