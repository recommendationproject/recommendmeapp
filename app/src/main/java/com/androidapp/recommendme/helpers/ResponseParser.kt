package com.androidapp.recommendme.helpers

import android.util.Log
import com.androidapp.recommendme.*
import com.example.recommendme.*
import org.json.JSONArray
import org.json.JSONObject

class ResponseParser {

    fun parseUserLoginResponse(response : JSONObject, isNewUser: Boolean?): User {
        val userId = response.getString("userid")
        val userDetails = response.getJSONObject("user_details")
        val email = userDetails.getString("email")
        val firstName = userDetails.getString("first_name")
        val lastName = userDetails.getString("last_name")

        var isNewUserValue = false
        if (isNewUser != null) {
            isNewUserValue = isNewUser
        }

        var interests = JSONArray()
        if (userDetails.has("interests") && userDetails.get("interests") != null && userDetails.get("interests") is JSONArray) {
            interests = userDetails.getJSONArray("interests")
        }
        val personalisations = userDetails.getJSONObject("personalisations")

        var interestsList = parseUserInterests(interests)
        var personalisationsList = parseUserPersonalisations(interestsList, personalisations)

        var profilePictureUrl = userDetails.getString("profile_picture")
        var facebookId = userDetails.getString("facebook_userid")
        var movieProfile = parseMovieProfile(userDetails.getJSONObject("movie_profile"))

        var remainingOnDemandMoviesCount = 0
        var onDemandMovieOptionsList = mutableListOf<OnDemandMovieOption>()
        if (userDetails.has("on_demand_movies_config")) {
            var onDemandMoviesConfig = userDetails.getJSONObject("on_demand_movies_config")
            remainingOnDemandMoviesCount = onDemandMoviesConfig.getInt("max_count") - onDemandMoviesConfig.getInt("used_this_month")
            var onDemandMovieOptionsJson = onDemandMoviesConfig.getJSONObject("options")
            
            for (key in onDemandMovieOptionsJson.keys()) {
                onDemandMovieOptionsList.add(OnDemandMovieOption(tag = key, text = onDemandMovieOptionsJson.getString(key)))
            }

        }

        return User(
            userId = userId,
            email = email,
            firstName = firstName,
            lastName = lastName,
            interestsCategories = interestsList,
            answeredPersonalisationQuestions = personalisationsList,
            facebookId = facebookId,
            profilePictureUrl = profilePictureUrl,
            movieProfile = movieProfile,
            isNewUser = isNewUserValue,
            remainingOnDemandMoviesCount = remainingOnDemandMoviesCount,
            onDemandMovieOptions = onDemandMovieOptionsList
        )
    }

    private fun parseMovieProfile(movieProfileJson: JSONObject): MovieProfile {
        val movieInterestRating = movieProfileJson.getInt("rating")
        val genreJSONArray = movieProfileJson.getJSONArray("genres")
        val languageJSONArray = movieProfileJson.getJSONArray("languages")
        val yearJSONArray = movieProfileJson.getJSONArray("years")
        var streamingPlatformJSONArray = JSONArray()
        if (movieProfileJson.has("streaming_platforms")) {
            streamingPlatformJSONArray = movieProfileJson.getJSONArray("streaming_platforms")
        }
        var sendSurpriseRecommendation = true
        if (movieProfileJson.has("send_surprise_reco")) {
            sendSurpriseRecommendation = movieProfileJson.getBoolean("send_surprise_reco")
        }


        var genreList: MutableList<CheckboxItem> = mutableListOf()
        for (i in 0 until genreJSONArray.length()) {
            val genreItem = genreJSONArray.getJSONObject(i)
            genreList.add(
                CheckboxItem(
                    text = genreItem.getString("display_value"),
                    property = genreItem.getString("property"),
                    isSelected = genreItem.getBoolean("selected")
                )
            )
        }

        var languageList: MutableList<CheckboxItem> = mutableListOf()
        for (i in 0 until languageJSONArray.length()) {
            val languageItem = languageJSONArray.getJSONObject(i)
            languageList.add(
                CheckboxItem(
                    text = languageItem.getString("display_value"),
                    property = languageItem.getString("property"),
                    isSelected = languageItem.getBoolean("selected")
                )
            )
        }

        var yearList: MutableList<CheckboxItem> = mutableListOf()
        for (i in 0 until yearJSONArray.length()) {
            val yearItem = yearJSONArray.getJSONObject(i)
            yearList.add(
                CheckboxItem(
                    text = yearItem.getString("display_value"),
                    property = yearItem.getString("property"),
                    isSelected = yearItem.getBoolean("selected")
                )
            )
        }

        var streamingPlatformsList: MutableList<String> = mutableListOf()
        for (i in 0 until streamingPlatformJSONArray.length()) {
            val streamingPlatform = streamingPlatformJSONArray.getString(i)
            streamingPlatformsList.add(streamingPlatform)
        }

        return MovieProfile(
            movieInterestRating = movieInterestRating,
            genreList = genreList,
            languageList = languageList,
            yearList = yearList,
            streamingPlatformList = streamingPlatformsList,
            sendSurpriseRecommendation = sendSurpriseRecommendation
        )
    }

    fun parseMovieDetails(movieDetails: JSONObject) : MovieRecommendation{
        var movie = MovieRecommendation()
        movie.title = movieDetails.getString("title")
        movie.description = movieDetails.getString("short_description")
        movie.plot = movieDetails.getString("plot")
        movie.primaryPhotoUrl = movieDetails.getString("primary_image")
        movie.rating = movieDetails.getString("rating")
        movie.releaseYear = movieDetails.getString("release_year")

        var genres_array = movieDetails.getJSONArray("genres")
        var genres_list = mutableListOf<String>()
        for (i in 0 until genres_array.length()) {
            genres_list.add(genres_array[i] as String)
        }
        movie.genres = genres_list

        movie.director = jsonArrayToString(movieDetails.getJSONArray("director"))
        movie.writer = jsonArrayToString(movieDetails.getJSONArray("writer"))
        movie.cast = jsonArrayToString(movieDetails.getJSONArray("actors"))
        movie.language = movieDetails.getString("language")
        movie.type = movieDetails.getString("type")
        movie.imdbId = movieDetails.getString("imdb_id")

        var featuredInJson = movieDetails.getJSONObject("featured_in")
        var featuredInList = mutableListOf<Link>()
        for (key in featuredInJson.keys()) {
            featuredInList.add(
                Link(
                    displayText = key,
                    url = featuredInJson.getString(key)
                )
            )
        }
        movie.featuredInList = featuredInList

        var availableAtJson = movieDetails.getJSONObject("available_at")
        var availableAtList = mutableListOf<Link>()
        for (key in availableAtJson.keys()) {
            availableAtList.add(
                Link(
                    displayText = key,
                    url = availableAtJson.getString(key)
                )
            )
        }
        movie.availableAtList = availableAtList

        return movie
    }

    private fun jsonArrayToString(array: JSONArray):String {
        var stringValue = ""
        for (i in 0 until array.length()) {
            stringValue += array.getString(i) + ", "
        }
        return stringValue.dropLast(2)
    }

    private fun parseUserInterests(interests: JSONArray) : List<String> {
        var interestsList = mutableListOf<String>()
        for (i in 0 until interests.length()) {
            interestsList.add(interests.getString(i))
        }
        return interestsList
    }

    private fun parseUserPersonalisations(interestsList: List<String>, personalisations: JSONObject) : List<UserInterestQuestion> {
        var personalisationsList = mutableListOf<UserInterestQuestion>()

        for (interest in interestsList) {
            if (personalisations.get(interest) != null && personalisations.getString(interest) != "null") {
                var interestPersonalisations = personalisations.getJSONArray(interest)

                for (j in 0 until interestPersonalisations.length()) {
                    var question = interestPersonalisations.getJSONObject(j)
                    var questionText = question.getString("question_text")
                    var category = question.getString("category")
                    var property = question.getString("property")

                    var answerOptionsList = mutableListOf<UserInterestAnswerOption>()

                    var answerOptions = question.getJSONArray("answer_options")

                    for (k in 0 until answerOptions.length()) {
                        var answerOption = answerOptions.getJSONObject(k)
                        var answerText = answerOption.getString("answer_text")
                        var answerProperty = answerOption.getString("property")
                        var isSelected = answerOption.getBoolean("selected")

                        answerOptionsList.add(
                            UserInterestAnswerOption(
                                text = answerText,
                                property = answerProperty,
                                isSelected = isSelected
                            )
                        )
                    }

                    personalisationsList.add(
                        UserInterestQuestion(
                            questionText = questionText,
                            questionProperty = property,
                            category = category,
                            options = answerOptionsList
                        )
                    )
                }
            }
        }

        return personalisationsList
    }
}