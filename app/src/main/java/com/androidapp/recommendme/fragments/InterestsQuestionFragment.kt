package com.androidapp.recommendme.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.androidapp.recommendme.Categories
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.UserInterestAnswerOption
import com.androidapp.recommendme.UserInterestQuestion
import com.example.recommendme.*
import com.androidapp.recommendme.adapters.InterestsQuestionAdapter
import kotlinx.android.synthetic.main.recycle_view_fragment.*
import org.json.JSONArray


class InterestsQuestionFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View = inflater.inflate(R.layout.recycle_view_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_recycle_view.apply {
            layoutManager = CustomLinearLayoutManager(
                context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = InterestsQuestionAdapter(
                context,
                mutableListOf(),
                layoutManager as CustomLinearLayoutManager
            )
        }

        PagerSnapHelper().attachToRecyclerView(fragment_recycle_view)
        getUserPersonalisationQuestions(context)
    }

    private fun getUserPersonalisationQuestions(context: Context?){
        val intent = (context as Activity).intent
        val extras = intent.extras
        val userId = extras?.getString("userId")

        var categories = (extras?.getSerializable("categories") as Categories).categories

        var questions: MutableList<UserInterestQuestion> = mutableListOf()
        ServiceVolley()
            .get("/user/personalisation?userid=" + userId) { response ->

            for (i in categories.indices) {
                val category = categories[i]
                val categoryQuestions = response?.get(category) as JSONArray

                for (i in 0 until categoryQuestions.length()) {
                    var question_object = categoryQuestions.getJSONObject(i)

                    var answerOptions = question_object.getJSONArray("answer_options")
                    var optionsList = mutableListOf<UserInterestAnswerOption>()

                    for (i in 0 until answerOptions.length()) {
                        var option = UserInterestAnswerOption(
                            property = answerOptions.getJSONObject(i).getString("property"),
                            text = answerOptions.getJSONObject(i).getString("answer_text"),
                            isSelected = answerOptions.getJSONObject(i).getBoolean("selected")
                        )
                        optionsList.add(option)
                    }

                    var question = UserInterestQuestion(
                        questionText = question_object.getString("question_text"),
                        questionProperty = question_object.getString("property"),
                        category = question_object.getString("category"),
                        options = optionsList
                    )
                    questions.add(question)
                }
            }
            fragment_recycle_view!!.adapter =
                InterestsQuestionAdapter(
                    context,
                    questions,
                    fragment_recycle_view.layoutManager as CustomLinearLayoutManager
                )
        }
    }
}

class CustomLinearLayoutManager(context: Context, orientation: Int, reverseLayout: Boolean):
    LinearLayoutManager(context, orientation, reverseLayout) {

    override fun canScrollHorizontally(): Boolean {
        return false
    }
}