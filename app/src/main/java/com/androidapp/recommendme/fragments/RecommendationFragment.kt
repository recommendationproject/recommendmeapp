package com.androidapp.recommendme.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.example.recommendme.R
import com.androidapp.recommendme.Recommendation
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.activities.RecommendationsActivity
import com.androidapp.recommendme.adapters.RecommendationsAdapter
import com.facebook.AccessToken
import kotlinx.android.synthetic.main.activity_recommendations.*
import kotlinx.android.synthetic.main.empty_recommendation_card.view.*
import kotlinx.android.synthetic.main.recycle_view_fragment.*
import org.json.JSONArray

class RecommendationFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.recycle_view_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_recycle_view.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = RecommendationsAdapter(
                context,
                mutableListOf(),
                "",
                ""
            )
        }
        PagerSnapHelper().attachToRecyclerView(fragment_recycle_view)
        getUserRecommendations(context)
    }

    private fun getUserRecommendations(context: Context?){
        val intent = (context as Activity).intent
        val extras = intent.extras
        val userId = extras?.getString("userId")
        var recommendationsType = extras?.getString("recommendations_type")
        var emptyRecoMessage = extras?.getString("empty_reco_message")

        var userRecommendations: MutableList<Recommendation> = mutableListOf()
        ServiceVolley()
            .get("/user/recommendations?userid=" + userId + "&access_token=" + AccessToken.getCurrentAccessToken().token + "&filter=" + recommendationsType) { response ->
            Log.d("RECOMM_RESPONSE", response.toString())
            val responseRecommendations = response?.get("recommendations")
            if (recommendationsType == null) {
                recommendationsType = ""
            }

            if (responseRecommendations != null && responseRecommendations is JSONArray && responseRecommendations.length() > 0 && activity!=null) {

                // Hide recommendations loading screen
                (activity as RecommendationsActivity).recommendations_loading_screen.visibility = View.GONE

                for (i in 0 until (responseRecommendations.length())) {
                    val recommendation = responseRecommendations.getJSONObject(i)
                    val category = recommendation.getString("category")
                    val title = recommendation.getString("title")
                    val shortDescription = recommendation.getString("short_description")
                    val recommendationId = recommendation.getString("id")
                    val isBookmarked = recommendation.getBoolean("is_bookmarked")

                    userRecommendations.add(
                        Recommendation(
                            category = category,
                            id = recommendationId,
                            isBookmarked = isBookmarked,
                            title = title,
                            description = shortDescription,
                            details_json = recommendation
                        )
                    )

                }
                fragment_recycle_view!!.adapter =
                    RecommendationsAdapter(
                        getContext(),
                        userRecommendations,
                        recommendationsType,
                        emptyRecoMessage
                    )
            } else if (activity!=null) {
                (activity as RecommendationsActivity).recommendations_loading_screen.visibility = View.GONE
                // add dummy reco
                userRecommendations.add(Recommendation())
                fragment_recycle_view!!.adapter =
                    RecommendationsAdapter(
                        getContext(),
                        userRecommendations,
                        recommendationsType,
                        emptyRecoMessage
                    )
            }
        }
    }
}
