import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.text.SpannableStringBuilder
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.text.bold
import com.bumptech.glide.Glide
import com.example.recommendme.*
import com.androidapp.recommendme.adapters.RecommendationViewHolder
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.movie_recommendation_card.view.*
import kotlinx.android.synthetic.main.movie_recommendation_card.view.recommendation_card_description
import kotlinx.android.synthetic.main.movie_recommendation_card.view.recommendation_card_title
import kotlinx.android.synthetic.main.movie_recommendation_card.view.recommendation_users_info
import kotlinx.android.synthetic.main.movie_recommendation_card.view.recommendation_users_info_picture
import kotlinx.android.synthetic.main.movie_recommendation_card.view.recommendation_users_info_text
import kotlinx.android.synthetic.main.rating_box.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import android.text.Spannable
import android.text.style.ForegroundColorSpan
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.animation.AlphaAnimation
import com.androidapp.recommendme.Link
import com.androidapp.recommendme.MovieRecommendation
import com.androidapp.recommendme.Recommendation
import com.androidapp.recommendme.RecommendationFriendsInfo


class MovieRecommendationViewHolder(itemView : View) : RecommendationViewHolder(itemView) {

    override fun bind(recommendation: Recommendation, position: Int, context: Context?) {

        if (recommendation.isBookmarked) {
            itemView.movie_bookmark_button.setImageResource(R.drawable.favorite_red)
        } else {
            itemView.movie_bookmark_button.setImageResource(R.drawable.favorite)
        }

        val movieDetails = parseMovieDetails(recommendation.details_json)

        setupPhotosSection(context, movieDetails)
        setupTitleSection(recommendation, context, movieDetails)
        setupRecommendationUsersInfo(recommendation, context)
        setupMovieDetailsSection(context, movieDetails)
        setupFeaturedInLayout(context, movieDetails)
        setupAvailableAtLayout(context, movieDetails)
    }

    private fun setupAvailableAtLayout(context: Context?, movieDetails: MovieRecommendation) {
        if (movieDetails.availableAtList.isNotEmpty()) {
            val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")
            itemView.movie_available_at_header_text.typeface = tf1

            for (link in movieDetails.availableAtList) {
                var animation = AlphaAnimation(1F, 0.2F)
                animation.duration = 500

                if (link.displayText == "Netflix"){
                    itemView.available_at_netflix.visibility = View.VISIBLE
                    itemView.available_at_netflix.setOnClickListener {
                        itemView.available_at_netflix.startAnimation(animation)
                        var uriIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link.url))
                        if (context != null) {
                            ContextCompat.startActivity(context, uriIntent, Bundle.EMPTY)
                        }
                    }
                } else if (link.displayText == "Prime Video") {
                    itemView.available_at_prime.visibility = View.VISIBLE
                    itemView.available_at_prime.setOnClickListener {
                        itemView.available_at_prime.startAnimation(animation)
                        var uriIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link.url))
                        if (context != null) {
                            ContextCompat.startActivity(context, uriIntent, Bundle.EMPTY)
                        }
                    }
                } else if (link.displayText == "Hotstar") {
                    itemView.available_at_hotstar.visibility = View.VISIBLE
                    itemView.available_at_hotstar.setOnClickListener {
                        itemView.available_at_hotstar.startAnimation(animation)
                        var uriIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link.url))
                        if (context != null) {
                            ContextCompat.startActivity(context, uriIntent, Bundle.EMPTY)
                        }
                    }
                } else if (link.displayText == "Zee5") {
                    itemView.available_at_zee5.visibility = View.VISIBLE
                    itemView.available_at_zee5.setOnClickListener {
                        itemView.available_at_zee5.startAnimation(animation)
                        var uriIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link.url))
                        if (context != null) {
                            ContextCompat.startActivity(context, uriIntent, Bundle.EMPTY)
                        }
                    }
                } else if (link.displayText == "Youtube Movies") {
                    itemView.available_at_youtube.visibility = View.VISIBLE
                    itemView.available_at_youtube.setOnClickListener {
                        itemView.available_at_youtube.startAnimation(animation)
                        var uriIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link.url))
                        if (context != null) {
                            ContextCompat.startActivity(context, uriIntent, Bundle.EMPTY)
                        }
                    }
                } else if (link.displayText == "Google Play") {
                    itemView.available_at_google_play.visibility = View.VISIBLE
                    itemView.available_at_google_play.setOnClickListener {
                        itemView.available_at_google_play.startAnimation(animation)
                        var uriIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link.url))
                        if (context != null) {
                            ContextCompat.startActivity(context, uriIntent, Bundle.EMPTY)
                        }
                    }
                }
           }

        } else {
            itemView.available_at_list_divider.visibility = View.GONE
            itemView.available_at_layout.visibility = View.GONE
        }
    }

    private fun setupFeaturedInLayout(context: Context?, movieDetails: MovieRecommendation) {
        if (movieDetails.featuredInList.isNotEmpty()) {
            val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")
            itemView.featured_in_header_text.typeface = tf1
            itemView.featured_in_list_text.typeface = tf1

            itemView.featured_in_list_text.movementMethod = LinkMovementMethod.getInstance()

            var featuredInListText = ""

            for (link in movieDetails.featuredInList) {
                featuredInListText += "<a href='" + link.url + "'>" + link.displayText + "</a><br/>"
            }

            featuredInListText = featuredInListText.dropLast(5)
            itemView.featured_in_list_text.text = Html.fromHtml(featuredInListText)

//            itemView.richLinkView.setLink("https://www.filmcompanion.in/fc-decades/pages/hindi.html", object : ViewListener {
//                override fun onSuccess(status: Boolean) {}
//                override fun onError(e: Exception) {}
//            })

//            val linkPreviewsAdapter = LinkPreviewsAdapter(context, movieDetails.featuredInList)
//            itemView.featured_in_list_previews.adapter = linkPreviewsAdapter
        } else {
            itemView.featured_in_list_divider.visibility = View.GONE
            itemView.movie_featured_in_layout.visibility = View.GONE
        }

    }

    private fun setupPhotosSection(context: Context?, movieDetails: MovieRecommendation) {
        Glide.with(context)
            .load(movieDetails.primaryPhotoUrl.replace("SX300", "SX2000"))
            .into(itemView.movie_poster_image)
            .waitForLayout()
    }

    private fun setupTitleSection(recommendation: Recommendation, context: Context?, movieDetails: MovieRecommendation) {
        val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")
        val tf2 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Italic.otf")

        itemView.recommendation_card_title.typeface = tf1
        itemView.recommendation_card_title.text = recommendation.title
        itemView.recommendation_card_description.typeface = tf2
        itemView.recommendation_card_description.text = recommendation.description
        if (recommendation.description == null || recommendation.description == "") {
            itemView.recommendation_card_description.visibility = View.GONE
        }

        //Subtitle
        itemView.movie_card_subtitle_text.typeface = tf1
        itemView.movie_card_subtitle_text.text = movieDetails.director + " | " + movieDetails.language.capitalize() + " | " + movieDetails.releaseYear

        // Rating details
        itemView.recommendation_rating_value.text = movieDetails.rating
        itemView.recommendation_rating_source.text = "IMDB"

        itemView.recommendation_rating.setOnClickListener {
            var imdbDeeplinkIntent = Intent(Intent.ACTION_VIEW, Uri.parse("imdb://title/" + movieDetails.imdbId))
            var imdbUrlIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.imdb.com/title/" + movieDetails.imdbId))
            if (context != null)
                try {
                    ContextCompat.startActivity(context,imdbDeeplinkIntent, Bundle.EMPTY)
                } catch (e: Exception) {
                    ContextCompat.startActivity(context,imdbUrlIntent, Bundle.EMPTY)
                }
        }

        setupGenres(context, movieDetails)
    }

    private fun setupGenres(context: Context?, movieDetails: MovieRecommendation) {
        val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")
        itemView.movie_card_genre_section_header.typeface = tf1

        itemView.movie_genres_chips.removeAllViews()
        var genres = movieDetails.genres

        for (genre in genres) {
            val movieGenreChip = Chip(context)
            movieGenreChip.setChipDrawable(ChipDrawable.createFromResource(context, R.xml.popular_about_chip))
            movieGenreChip.text = genre
            movieGenreChip.isClickable = false
            itemView.movie_genres_chips.addView(movieGenreChip)
        }
    }

    private fun setupMovieDetailsSection(context: Context?, movieDetails: MovieRecommendation) {
        val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Regular.otf")
        val tf2 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")

        itemView.movie_description_text.typeface = tf2
        itemView.movie_cast_and_crew_text.typeface = tf2
        itemView.movie_plot_text.typeface = tf1
        itemView.movie_director_text.typeface = tf1
        itemView.movie_writer_text.typeface = tf1
        itemView.movie_cast_text.typeface = tf1

        //itemView.movie_plot_text.text = SpannableStringBuilder().bold { append("Plot: ") }.append(movieDetails.plot)
        itemView.movie_plot_text.text = movieDetails.plot
        itemView.movie_director_text.text = SpannableStringBuilder().bold { append("Directed by: ") }.append(movieDetails.director)
        itemView.movie_writer_text.text = SpannableStringBuilder().bold { append("Written by: ") }.append(movieDetails.writer)
        itemView.movie_cast_text.text = SpannableStringBuilder().bold { append("Cast: ") }.append(movieDetails.cast)
    }

    private fun setupRecommendationUsersInfo(recommendation: Recommendation, context: Context?) {
        val tf4 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")

        var recommendationUsersInfo = parseRecommendationFriendsInfo(recommendation.details_json)
        itemView.recommendation_users_info_text.typeface = tf4
        if (recommendationUsersInfo.total_recommendations_count == 0) {
            itemView.recommendation_users_info.visibility = View.GONE
            itemView.recommendations_user_info_divider.visibility = View.GONE
        } else {
            Glide.with(context)
                .load(recommendationUsersInfo.friend_picture_url)
                .into(itemView.recommendation_users_info_picture)
                .waitForLayout()
            if (recommendationUsersInfo.friend_names != null && recommendationUsersInfo.friend_names.size > 0) {
                itemView.recommendation_users_info_text.text = recommendationUsersInfo.friend_names[0] + " and " +
                        (recommendationUsersInfo.total_recommendations_count-1).toString() + " others have recommended it."
            } else {
                if (recommendationUsersInfo.total_recommendations_count == 1)
                    itemView.recommendation_users_info_text.text = "1 person has recommended it."
                else
                    itemView.recommendation_users_info_text.text = recommendationUsersInfo.total_recommendations_count.toString() + " people have recommended it."
            }
        }
    }

    private fun parseRecommendationFriendsInfo(movie_details:JSONObject): RecommendationFriendsInfo {
        var info = RecommendationFriendsInfo()

        if (movie_details.has("recommendation_users_info")) {
            val infoJson = movie_details.getJSONObject("recommendation_users_info")

            info.total_recommendations_count = infoJson.getInt("total_recommendations_count")

            var friendsNames = infoJson.getJSONArray("friends_names")
            var friendsNamesList = mutableListOf<String>()
            for (i in 0 until friendsNames.length()) {
                friendsNamesList.add(friendsNames.getString(i))
            }
            info.friend_names = friendsNamesList
            info.friend_picture_url = infoJson.getString("picture_url")

        }
        return info
    }

    private fun parseMovieDetails(movieDetails:JSONObject): MovieRecommendation {
        var movie = MovieRecommendation()
        movie.title = movieDetails.getString("title")
        movie.description = movieDetails.getString("short_description")
        movie.plot = movieDetails.getString("plot")
        movie.primaryPhotoUrl = movieDetails.getString("primary_image")
        movie.rating = movieDetails.getString("rating")
        movie.releaseYear = movieDetails.getString("release_year")

        var genres_array = movieDetails.getJSONArray("genres")
        var genres_list = mutableListOf<String>()
        for (i in 0 until genres_array.length()) {
            genres_list.add(genres_array[i] as String)
        }
        movie.genres = genres_list

        movie.director = jsonArrayToString(movieDetails.getJSONArray("director"))
        movie.writer = jsonArrayToString(movieDetails.getJSONArray("writer"))
        movie.cast = jsonArrayToString(movieDetails.getJSONArray("actors"))
        movie.language = movieDetails.getString("language")
        movie.type = movieDetails.getString("type")
        movie.imdbId = movieDetails.getString("imdb_id")

        var featuredInJson = movieDetails.getJSONObject("featured_in")
        var featuredInList = mutableListOf<Link>()
        for (key in featuredInJson.keys()) {
            featuredInList.add(
                Link(
                    displayText = key,
                    url = featuredInJson.getString(key)
                )
            )
        }
        movie.featuredInList = featuredInList

        var availableAtJson = movieDetails.getJSONObject("available_at")
        var availableAtList = mutableListOf<Link>()
        for (key in availableAtJson.keys()) {
            availableAtList.add(
                Link(
                    displayText = key,
                    url = availableAtJson.getString(key)
                )
            )
        }
        movie.availableAtList = availableAtList

        return movie
    }

    private fun jsonArrayToString(array:JSONArray):String {
        var stringValue = ""
        for (i in 0 until array.length()) {
            stringValue += array.getString(i) + ", "
        }
        return stringValue.dropLast(2)
    }

}
