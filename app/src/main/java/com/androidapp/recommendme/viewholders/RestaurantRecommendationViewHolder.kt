import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.androidapp.recommendme.Recommendation
import com.androidapp.recommendme.RecommendationFriendsInfo
import com.androidapp.recommendme.RestaurantLocation
import com.androidapp.recommendme.RestaurantRecommendation
import com.bumptech.glide.Glide
import com.example.recommendme.*
import com.androidapp.recommendme.adapters.RecommendationViewHolder
import com.google.android.material.card.MaterialCardView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.active_recommendation_card.view.*
import kotlinx.android.synthetic.main.active_recommendation_card.view.recommendation_card_description
import kotlinx.android.synthetic.main.active_recommendation_card.view.recommendation_card_title
import kotlinx.android.synthetic.main.restaurant_card_options.view.*
import kotlinx.android.synthetic.main.restaurant_photo_item.view.*
import org.json.JSONObject
import java.lang.Exception

class RestaurantRecommendationViewHolder(itemView : View) : RecommendationViewHolder(itemView) {

    override fun bind(recommendation: Recommendation, position: Int, context: Context?) {

        if (recommendation.isBookmarked) {
            itemView.restaurant_bookmark_button.setImageResource(R.drawable.favorite_red)
        } else {
            itemView.restaurant_bookmark_button.setImageResource(R.drawable.favorite)
        }

        val restaurantDetails = parseRestaurantDetails(recommendation.details_json)

        setupPhotosSection(context, restaurantDetails)
        setupTitleSection(recommendation, context, restaurantDetails)
        setupPopularAboutSection(recommendation, context, restaurantDetails)
        setupRecommendationUsersInfo(recommendation, context)
        setupTopDishesSection(context, restaurantDetails)
        setupRestaurantOptions(context, restaurantDetails)
    }

    private fun setupPhotosSection(context: Context?, restaurantDetails: RestaurantRecommendation) {
        Glide.with(context)
            .load(restaurantDetails.primaryPhotoUrl)
            .into(itemView.recommendation_card_image)
            .waitForLayout()

        var photosList = mutableListOf<String>()
        photosList.add(restaurantDetails.primaryPhotoUrl)
        for (photo in restaurantDetails.photos) {
            if (photosList.size < 6) {
                photosList.add(photo)
            }
        }

        for (i in 0 until photosList.size) {
            var inflater = LayoutInflater.from(context)
            if (context != null) {
                val scale = context.resources.displayMetrics.density
                val layoutParams = LinearLayout.LayoutParams(
                    (52 * scale + 0.5f).toInt(),
                    LinearLayout.LayoutParams.MATCH_PARENT
                )
                layoutParams.setMargins((4 * scale + 0.5f).toInt(), 0, 0, 0)
                var restaurantPhoto = inflater.inflate(R.layout.restaurant_photo_item, null, false) as MaterialCardView

                Glide.with(context)
                    .load(photosList.get(i))
                    .into(restaurantPhoto.restaurant_photo_thumbnail)
                    .waitForLayout()

                restaurantPhoto.setOnClickListener {
                    Glide.with(context)
                        .load(photosList.get(i))
                        .into(itemView.recommendation_card_image)
                        .waitForLayout()
                }
                itemView.restaurant_photos.addView(restaurantPhoto, layoutParams)
            }
        }
    }

    private fun setupTitleSection(recommendation: Recommendation, context: Context?, restaurantDetails: RestaurantRecommendation) {
        val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")
        val tf2 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Italic.otf")

        itemView.recommendation_card_title.typeface = tf1
        itemView.recommendation_card_title.text = recommendation.title
        itemView.recommendation_card_description.typeface = tf2
        itemView.recommendation_card_description.text = recommendation.description

        // Rating details
        itemView.restaurant_rating_value.text = restaurantDetails.rating
        itemView.restaurant_rating_value.setBackgroundColor(Color.parseColor("#" + restaurantDetails.ratingColor))
    }

    private fun setupRecommendationUsersInfo(recommendation: Recommendation, context: Context?) {
        val tf4 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")

        var recommendationUsersInfo = parseRecommendationFriendsInfo(recommendation.details_json)
        itemView.recommendation_users_info_text.typeface = tf4
        if (recommendationUsersInfo.total_recommendations_count == 0) {
            itemView.recommendation_users_info.visibility = View.GONE
        } else {
            Glide.with(context)
                .load(recommendationUsersInfo.friend_picture_url)
                .into(itemView.recommendation_users_info_picture)
                .waitForLayout()
            if (recommendationUsersInfo.total_recommendations_count == 1)
                itemView.recommendation_users_info_text.text = "1 person has recommended it."
            else
                itemView.recommendation_users_info_text.text = recommendationUsersInfo.total_recommendations_count.toString() + " people have recommended it."
        }
    }

    private fun setupPopularAboutSection(recommendation: Recommendation, context: Context?, restaurantDetails: RestaurantRecommendation) {
        val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")

        itemView.popular_about_restaurant_section_text.typeface = tf1
        itemView.popular_about_restaurant_section_text.text = "Popular about " + recommendation.title
        var popularAboutRestaurantGroup = itemView.popular_about_restaurant_chips as ChipGroup
        val highlights = restaurantDetails.highlights.split(",").toList()
        for (i in 0 until highlights.size) {
            val popularAboutRestaurantChip = Chip(context)
            popularAboutRestaurantChip.setChipDrawable(ChipDrawable.createFromResource(context, R.xml.popular_about_chip))
            popularAboutRestaurantChip.setTextAppearanceResource(R.style.ChipTextStyle)
            popularAboutRestaurantChip.text = highlights[i]
            popularAboutRestaurantChip.isClickable = false
            popularAboutRestaurantGroup.addView(popularAboutRestaurantChip)
        }
    }

    private fun setupTopDishesSection(context: Context?, restaurantDetails: RestaurantRecommendation) {
        val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")

        itemView.top_dishes_section_text.typeface = tf1
        var topDishesGroup = itemView.top_dishes_section_chips as ChipGroup
        val topDishes = restaurantDetails.highlights.split(",").toList()
        for (i in 0 until topDishes.size) {
            val topDishChip = Chip(context)
            topDishChip.setChipDrawable(ChipDrawable.createFromResource(context, R.xml.top_things_chip))
            topDishChip.setTextAppearanceResource(R.style.ChipTextStyle)
            topDishChip.text = topDishes[i]
            topDishChip.isClickable = false
            topDishesGroup.addView(topDishChip)
        }
    }

    private fun setupRestaurantOptions(context: Context?, restaurantDetails: RestaurantRecommendation) {
        setupZomatoLink(context, restaurantDetails)
        setupGoogleMapsLink(context, restaurantDetails)
    }

    private fun setupZomatoLink(context: Context?, restaurantDetails: RestaurantRecommendation) {
        itemView.restaurant_actions.zomato_restaurant_link.setOnClickListener {
            var zomatoDeeplinkIntent = Intent(Intent.ACTION_VIEW, Uri.parse(restaurantDetails.deeplink))
            var zomatoUrlIntent = Intent(Intent.ACTION_VIEW, Uri.parse(restaurantDetails.url))
            if (context != null)
                try {
                    ContextCompat.startActivity(context,zomatoDeeplinkIntent, Bundle.EMPTY)
                } catch (e: Exception) {
                    ContextCompat.startActivity(context,zomatoUrlIntent, Bundle.EMPTY)
                }
        }
    }

    private fun setupGoogleMapsLink(context: Context?, restaurantDetails: RestaurantRecommendation) {
        itemView.restaurant_actions.open_google_maps.setOnClickListener {
            val latitude = restaurantDetails.location.latitude
            val longitude = restaurantDetails.location.longitude
            var googleMapIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + latitude + "," + longitude))
            googleMapIntent.setPackage("com.google.android.apps.maps")
            if (context != null)
                ContextCompat.startActivity(context, googleMapIntent, Bundle.EMPTY)
        }
    }

    private fun parseRecommendationFriendsInfo(restaurant_details:JSONObject): RecommendationFriendsInfo {
        var info = RecommendationFriendsInfo()

        if (restaurant_details.has("recommendation_users_info")) {
            val infoJson = restaurant_details.getJSONObject("recommendation_users_info")

            info.total_recommendations_count = infoJson.getInt("total_recommendations_count")

            var friendsNames = infoJson.getJSONArray("friends_names")
            var friendsNamesList = mutableListOf<String>()
            for (i in 0 until friendsNames.length()) {
                friendsNamesList.add(friendsNames.getString(i))
            }
            info.friend_names = friendsNamesList
            info.friend_picture_url = infoJson.getString("picture_url")
        }

        return info
    }

    private fun parseRestaurantDetails(restaurant_details:JSONObject): RestaurantRecommendation {
        var restaurant = RestaurantRecommendation()
        restaurant.title = restaurant_details.getString("title")
        restaurant.description = restaurant_details.getString("short_description")
        restaurant.deeplink = restaurant_details.getString("restaurant_deeplink")
        restaurant.primaryPhotoUrl = restaurant_details.getString("primary_image")

        val ratingObject:JSONObject = restaurant_details.getJSONObject("user_rating")
        restaurant.rating = ratingObject.getString("aggregate_rating")
        restaurant.ratingCount = ratingObject.getString("votes")
        restaurant.ratingColor = ratingObject.getString("rating_color")

        val locationObject:JSONObject = restaurant_details.getJSONObject("location")
        var location = RestaurantLocation()
        location.address = locationObject.getString("address")
        location.city = locationObject.getString("city")
        location.longitude = locationObject.getString("longitude")
        location.latitude = locationObject.getString("latitude")

        restaurant.location = location
        restaurant.highlights = restaurant_details.getString("cuisines")
        restaurant.url = restaurant_details.getString("restaurant_url")

        var photosArray = restaurant_details.getJSONArray("photos")
        var photosList = mutableListOf<String>()
        for (i in 0 until photosArray.length()) {
            photosList.add(photosArray.getString(i))
        }
        restaurant.photos = photosList

        return restaurant
    }

}
