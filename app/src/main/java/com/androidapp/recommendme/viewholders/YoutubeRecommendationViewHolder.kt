import android.content.Context
import android.view.View
import com.androidapp.recommendme.Recommendation
import com.example.recommendme.*
import com.androidapp.recommendme.adapters.RecommendationViewHolder
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubeThumbnailLoader
import com.google.android.youtube.player.YouTubeThumbnailView
import kotlinx.android.synthetic.main.active_recommendation_card.view.recommendation_card_description
import kotlinx.android.synthetic.main.active_recommendation_card.view.recommendation_card_title
import kotlinx.android.synthetic.main.youtube_video_recommendation_card.view.*

class YoutubeRecommendationViewHolder(itemView : View) : RecommendationViewHolder(itemView) {

    private val VIDEO_ID: String = "VOLKJJvfAbg"
    private val YOUTUBE_API_KEY: String = "AIzaSyAGYLzqvpEt3RECmwamycY217516f6frug"

    override fun bind(recommendation: Recommendation, position: Int, context: Context?) {

        itemView.youtube_card_scroll_view.visibility = View.GONE
        itemView.youtube_card_buttons.visibility = View.GONE
        itemView.youtube_card_loading_view.visibility = View.VISIBLE

        itemView.recommendation_card_title.text = recommendation.title
        itemView.recommendation_card_description.text = recommendation.description

        itemView.youtubeThumbnailView.initialize(YOUTUBE_API_KEY, object : YouTubeThumbnailView.OnInitializedListener {
            override fun onInitializationSuccess(p0: YouTubeThumbnailView?, ytThumbnailLoader: YouTubeThumbnailLoader?) {
                ytThumbnailLoader?.setVideo(VIDEO_ID)
                ytThumbnailLoader?.setOnThumbnailLoadedListener(object : YouTubeThumbnailLoader.OnThumbnailLoadedListener {
                    override fun onThumbnailLoaded(p0: YouTubeThumbnailView?, p1: String?) {
                        ytThumbnailLoader?.release()

                        itemView.youtube_card_scroll_view.visibility = View.VISIBLE
                        itemView.youtube_card_buttons.visibility = View.VISIBLE
                        itemView.youtube_card_loading_view.visibility = View.GONE

                    }

                    override fun onThumbnailError(p0: YouTubeThumbnailView?, p1: YouTubeThumbnailLoader.ErrorReason?) {
                    }

                })
            }
            override fun onInitializationFailure(p0: YouTubeThumbnailView?, p1: YouTubeInitializationResult?) {

            }
        })
    }
}
