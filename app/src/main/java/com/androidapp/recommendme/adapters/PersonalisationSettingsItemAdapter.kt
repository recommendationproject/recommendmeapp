package com.androidapp.recommendme.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import com.example.recommendme.R
import com.androidapp.recommendme.UserInterestQuestion
import kotlinx.android.synthetic.main.personalization_settings_item.view.*

class PersonalisationSettingsItemAdapter(private val context:Context?, private val questions: List<UserInterestQuestion>): BaseAdapter() {

    private val inflater: LayoutInflater
            = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    fun updateQuestions() {
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return questions.size
    }

    override fun getItem(position: Int): Any {
        return questions[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.personalization_settings_item, parent, false) as LinearLayout

        val question = getItem(position) as UserInterestQuestion

        rowView.ps_question_title_text.text = question.questionText
        for (answer in question.options) {
            if (answer.isSelected) {
                rowView.ps_answer_text.text = answer.text
                break
            }
        }

        return rowView
    }
}