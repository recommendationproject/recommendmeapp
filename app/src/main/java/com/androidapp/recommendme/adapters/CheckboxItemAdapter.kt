package com.androidapp.recommendme.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import com.androidapp.recommendme.CheckboxItem
import com.example.recommendme.R
import kotlinx.android.synthetic.main.checkbox_item.view.*

class CheckboxItemAdapter(private val context:Context?, private var optionsList: MutableList<CheckboxItem>): BaseAdapter() {

    private val inflater: LayoutInflater
            = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return optionsList.size
    }

    override fun getItem(position: Int): Any {
        return optionsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.checkbox_item, parent, false) as CheckBox
        val checkboxItem = getItem(position) as CheckboxItem
        rowView.checkbox_item.text = checkboxItem.text
        rowView.checkbox_item.isChecked = checkboxItem.isSelected

        rowView.setOnClickListener {
            optionsList.get(position).isSelected = !optionsList.get(position).isSelected
        }
        return rowView
    }
}