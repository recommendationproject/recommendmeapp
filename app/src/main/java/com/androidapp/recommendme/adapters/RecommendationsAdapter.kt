package com.androidapp.recommendme.adapters

import MovieRecommendationViewHolder
import RestaurantRecommendationViewHolder
import YoutubeRecommendationViewHolder
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.active_recommendation_card.view.*
import kotlinx.android.synthetic.main.active_recommendation_card.view.recommendation_card_description
import kotlinx.android.synthetic.main.active_recommendation_card.view.recommendation_card_title
import kotlinx.android.synthetic.main.youtube_video_recommendation_card.view.*
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.example.recommendme.*
import com.google.android.material.card.MaterialCardView
import com.google.android.youtube.player.*
import kotlinx.android.synthetic.main.restaurant_card_options.view.*
import kotlinx.android.synthetic.main.restaurant_photo_item.view.*
import kotlinx.android.synthetic.main.youtube_video_recommendation_card.view.youtubeThumbnailView
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.lang.Exception
import android.R.attr.bitmap
import android.provider.MediaStore
import android.R.attr.bitmap
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.util.TypedValue
import android.view.animation.AlphaAnimation
import android.widget.Toast
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.FileProvider
import com.androidapp.recommendme.Recommendation
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.activities.OnDemandRecommendationsActivity
import com.facebook.AccessToken
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.empty_recommendation_card.view.*
import kotlinx.android.synthetic.main.movie_card_options.view.*
import kotlinx.android.synthetic.main.movie_recommendation_card.view.*
import kotlinx.android.synthetic.main.restaurant_card_options.view.not_recommend_button
import kotlinx.android.synthetic.main.restaurant_card_options.view.recommend_button
import kotlinx.android.synthetic.main.restaurant_card_options.view.restaurant_actions
import kotlinx.android.synthetic.main.restaurant_card_options.view.share_recommendation
import java.io.File
import java.io.FileOutputStream


class RecommendationsAdapter(val context: Context?, private var recommendations: MutableList<Recommendation>, var recommendationType: String?, var emptyRecoMessage: String?):
    RecyclerView.Adapter<RecommendationViewHolder>() {

    private val youtubeCard = 0
    private val restaurantCard = 1
    private val movieCard = 2

    private val VIDEO_ID: String = "VOLKJJvfAbg"
    private val YOUTUBE_API_KEY: String = "AIzaSyAGYLzqvpEt3RECmwamycY217516f6frug"


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendationViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val view: View

        if (viewType >= 0) {
            view = inflater.inflate(R.layout.movie_recommendation_view, parent, false)
            return MovieRecommendationViewHolder(view)
        } else {
            view = inflater.inflate(R.layout.empty_recommendation_card, parent, false)
            if (recommendationType == "bookmarks") {
                view.empty_recommendation_text_view.text = "No Bookmarked recommendations available"
                view.get_on_demand_movies_button.visibility = View.GONE
            } else if (emptyRecoMessage != null && emptyRecoMessage != "") {
                view.empty_recommendation_text_view.text = emptyRecoMessage
                view.get_on_demand_movies_button.visibility = View.GONE
            }

            view.get_on_demand_movies_button.setOnClickListener {
                val intent = (context as Activity).intent
                val extras = intent.extras
                val user = extras?.getSerializable("user")
                var onDemandIntent = Intent(context, OnDemandRecommendationsActivity::class.java)
                onDemandIntent.putExtra("user", user)
                startActivityForResult(context, onDemandIntent, 1, null)
            }
        }
//        when(viewType) {
//            youtubeCard -> {
//                view = inflater.inflate(R.layout.youtube_video_recommendation_card, parent, false)
//                return YoutubeRecommendationViewHolder(view)
//            }
//            restaurantCard -> {
//                view = inflater.inflate(R.layout.restaurant_recommendation_view, parent, false)
//                return RestaurantRecommendationViewHolder(view)
//            }
//            movieCard -> {
//                view = inflater.inflate(R.layout.movie_recommendation_view, parent, false)
//                return MovieRecommendationViewHolder(view)
//            }
//            else -> {
//                view = inflater.inflate(R.layout.empty_recommendation_card, parent, false)
//                if (recommendationType == "bookmarks") {
//                    view.empty_recommendation_text_view.text = "No Bookmarked recommendations available"
//                    view.get_on_demand_movies_button.visibility = View.GONE
//                } else if (emptyRecoMessage != null && emptyRecoMessage != "") {
//                    view.empty_recommendation_text_view.text = emptyRecoMessage
//                    view.get_on_demand_movies_button.visibility = View.GONE
//                }
//
//                view.get_on_demand_movies_button.setOnClickListener {
//                    val intent = (context as Activity).intent
//                    val extras = intent.extras
//                    val user = extras?.getSerializable("user")
//                    var onDemandIntent = Intent(context, OnDemandRecommendationsActivity::class.java)
//                    onDemandIntent.putExtra("user", user)
//                    startActivityForResult(context, onDemandIntent, 1, null)
//                }
//            }
//        }
        return RecommendationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return recommendations.size
    }
    
    override fun onBindViewHolder(holder: RecommendationViewHolder, position: Int) {
        val recommendation: Recommendation = recommendations[position]
        holder.bind(recommendation, position, context)

        if (recommendation.category == "youtube") {
            holder.itemView.play_button.setOnClickListener {
                holder.itemView.youtubeThumbnailView.visibility = View.GONE
                holder.itemView.play_button.visibility = View.GONE
                val youtubePlayerFragment = YouTubePlayerFragment.newInstance()
                val youtubePlayerInit = object : YouTubePlayer.OnInitializedListener {
                    override fun onInitializationSuccess(
                        p0: YouTubePlayer.Provider?,
                        youtubePlayer: YouTubePlayer?,
                        p2: Boolean
                    ) {
                        youtubePlayer?.loadVideo(VIDEO_ID)
                        youtubePlayer?.setShowFullscreenButton(false)
                    }

                    override fun onInitializationFailure(
                        p0: YouTubePlayer.Provider?,
                        p1: YouTubeInitializationResult?
                    ) {
                    }
                }

                (context as Activity).fragmentManager.beginTransaction()
                    .replace(holder.itemView.youtube_holder.id, youtubePlayerFragment).commit()
                youtubePlayerFragment.initialize(YOUTUBE_API_KEY, youtubePlayerInit)
            }
        } else if (recommendation.category == "restaurant") {
            val intent = (context as Activity).intent
            val extras = intent.extras
            val userId = extras?.getString("userId")

            holder.itemView.restaurant_actions.recommend_button.setOnClickListener {

                /*var animation = AlphaAnimation(1F, 0.2F)
                holder.itemView.recommend_button.startAnimation(animation)*/

                ServiceVolley().post(
                    "/user/recommendation?action=recommend&userid=" + userId + "&recommendationid=" + recommendation.id + "&recommendation_type=" + "restaurant",
                    JSONObject()
                ) { response ->
                    Log.d("REST_RECOMMEND", response.toString())
                    if (response!=null) {
                        // Trying animation
                        var endAction = object : Runnable {
                            override fun run() {
                                recommendations.removeAt(position)
                                notifyDataSetChanged()
                                holder.itemView.animate().setDuration(0).alpha(1.0f).start()
                            }
                        }
                        holder.itemView.animate().setDuration(1000).alpha(0.0f).withEndAction(endAction)
                            .start()
                    }
                }
            }

            holder.itemView.restaurant_actions.not_recommend_button.setOnClickListener {
                // call not recommendation api
                ServiceVolley().post(
                    "/user/recommendation?action=not_recommend&userid=" + userId + "&recommendationid=" + recommendation.id + "&recommendation_type=" + "restaurant",
                    JSONObject()
                ) { response ->
                    Log.d("REST_NOT_RECOMMEND", response.toString())
                    if (response != null) {
                        // Trying animation
                        var endAction = object : Runnable {
                            override fun run() {
                                recommendations.removeAt(position)
                                notifyDataSetChanged()
                                holder.itemView.animate().setDuration(0).alpha(1.0f).start()
                            }
                        }
                        holder.itemView.animate().setDuration(1000).alpha(0.0f).withEndAction(endAction).start()
                    }
                }

            }

            holder.itemView.restaurant_bookmark_button.setOnClickListener {
                if (recommendation.isBookmarked) {
                    ServiceVolley().post(
                        "/user/recommendation?action=remove&userid=" + userId + "&recommendationid=" + recommendation.id + "&recommendation_type=" + "restaurant",
                        JSONObject()
                    ) { response ->
                        Log.d("REST_BOOKMARK_REMOVE", response.toString())
                        recommendation.isBookmarked = false
                        holder.itemView.restaurant_bookmark_button.setImageResource(R.drawable.favorite)
                    }
                } else {
                    ServiceVolley().post(
                        "/user/recommendation?action=bookmark&userid=" + userId + "&recommendationid=" + recommendation.id + "&recommendation_type=" + "restaurant",
                        JSONObject()
                    ) { response ->
                        Log.d("REST_BOOKMARK", response.toString())
                        recommendation.isBookmarked = true
                        holder.itemView.restaurant_bookmark_button.setImageResource(R.drawable.favorite_red)
                    }
                }
            }

            holder.itemView.restaurant_actions.share_recommendation.setOnClickListener {
                if (context != null) {

                    var screenshot:Bitmap = getScreenShot(holder.itemView)

                    val file = File(context.externalCacheDir, "logicchip.png")
                    val fOut = FileOutputStream(file)
                    screenshot.compress(Bitmap.CompressFormat.PNG, 100, fOut)
                    fOut.flush()
                    fOut.close()
                    file.setReadable(true, false)

                    var photoURI = FileProvider.getUriForFile(context,
                        "com.example.recommendme.fileprovider",
                        file)

                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_STREAM, photoURI)
                        putExtra(Intent.EXTRA_SUBJECT, "Recommendations")
                        putExtra(Intent.EXTRA_TEXT, "Checkout this restaurant recommendation from RecommendMe. Download app at: <Link>")
                        type = "image/*"
                    }
                    sendIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

                    val shareIntent = Intent.createChooser(sendIntent, "Share recommendation")
                    ContextCompat.startActivity(context, shareIntent, Bundle.EMPTY)
                }
            }
        } else if (recommendation.category == "movie") {
            val intent = (context as Activity).intent
            val extras = intent.extras
            val userId = extras?.getString("userId")

            holder.itemView.movie_actions.recommend_button.setOnClickListener {
                ServiceVolley().post(
                    "/user/recommendation?action=recommend&userid=" + userId + "&access_token=" + AccessToken.getCurrentAccessToken().token + "&recommendationid=" + recommendation.id + "&recommendation_type=" + "movie",
                    JSONObject()
                ) { response ->
                    Log.d("REST_RECOMMEND", response.toString())
                    if (response != null) {
                        Toast.makeText(this.context, "Recommendation Submitted", Toast.LENGTH_SHORT).show()
                    }
                }
                // Trying animation
                var endAction = object : Runnable {
                    override fun run() {
                        recommendations.removeAt(position)

                        if (recommendations.size == 0) {
                            recommendations.add(Recommendation())
                        }

                        notifyDataSetChanged()
                        holder.itemView.animate().setDuration(0).alpha(1.0f).start()
                    }
                }
                holder.itemView.animate().setDuration(1000).alpha(0.0f).withEndAction(endAction)
                    .start()
            }

            holder.itemView.movie_actions.not_recommend_button.setOnClickListener {
                // call not recommendation api
                ServiceVolley().post(
                    "/user/recommendation?action=not_recommend&userid=" + userId + "&access_token=" + AccessToken.getCurrentAccessToken().token + "&recommendationid=" + recommendation.id + "&recommendation_type=" + "movie",
                    JSONObject()
                ) { response ->
                    Log.d("REST_NOT_RECOMMEND", response.toString())
                    if (response != null) {
                        Toast.makeText(this.context, "Recommendation Submitted", Toast.LENGTH_SHORT).show()
                    }
                }
                // Trying animation
                var endAction = object : Runnable {
                    override fun run() {
                        recommendations.removeAt(position)

                        if (recommendations.size == 0) {
                            recommendations.add(Recommendation())
                        }
                        notifyDataSetChanged()
                        holder.itemView.animate().setDuration(0).alpha(1.0f).start()
                    }
                }
                holder.itemView.animate().setDuration(1000).alpha(0.0f).withEndAction(endAction)
                    .start()
            }

            holder.itemView.movie_bookmark_button.setOnClickListener {
                if (recommendation.isBookmarked) {
                    ServiceVolley().post(
                        "/user/recommendation?action=remove&userid=" + userId + "&access_token=" + AccessToken.getCurrentAccessToken().token + "&recommendationid=" + recommendation.id + "&recommendation_type=" + "movie",
                        JSONObject()
                    ) { response ->
                        Log.d("REST_BOOKMARK_REMOVE", response.toString())
                    }
                    var animation = AlphaAnimation(1F, 0.2F)
                    animation.duration = 500
                    holder.itemView.movie_bookmark_button.startAnimation(animation)
                    recommendation.isBookmarked = false
                    holder.itemView.movie_bookmark_button.setImageResource(R.drawable.favorite)
                } else {
                    ServiceVolley().post(
                        "/user/recommendation?action=bookmark&userid=" + userId + "&access_token=" + AccessToken.getCurrentAccessToken().token + "&recommendationid=" + recommendation.id + "&recommendation_type=" + "movie",
                        JSONObject()
                    ) { response ->
                        Log.d("REST_BOOKMARK", response.toString())
                    }
                    var animation = AlphaAnimation(1F, 0.2F)
                    animation.duration = 500
                    holder.itemView.movie_bookmark_button.startAnimation(animation)
                    recommendation.isBookmarked = true
                    holder.itemView.movie_bookmark_button.setImageResource(R.drawable.favorite_red)
                }
            }

            holder.itemView.movie_actions.share_recommendation.setOnClickListener {
                if (context != null) {

                    var screenshot:Bitmap = getScreenShot(holder.itemView.movie_recommendation_frame_view)

                    val file = File(context.externalCacheDir, "MovieRecommendation.png")
                    val fOut = FileOutputStream(file)
                    screenshot.compress(Bitmap.CompressFormat.PNG, 100, fOut)
                    fOut.flush()
                    fOut.close()
                    file.setReadable(true, false)

                    var photoURI = FileProvider.getUriForFile(context,
                        "com.example.recommendme.fileprovider",
                        file)

                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_STREAM, photoURI)
                        putExtra(Intent.EXTRA_SUBJECT, "Movie recommendation")
                        putExtra(Intent.EXTRA_TEXT, "Checkout this movie recommendation from RecommendMe. Download app at: https://play.google.com/store/apps/details?id=com.androidapp.recommendme")
                        type = "image/*"
                    }
                    sendIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

                    val shareIntent = Intent.createChooser(sendIntent, "Share movie recommendation")
                    ContextCompat.startActivity(context, shareIntent, Bundle.EMPTY)
                }
            }

            holder.itemView.movie_actions.watch_movie_trailer_button.setOnClickListener {

            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(recommendations[position].category) {
//            "youtube" -> youtubeCard
//            "restaurant" -> restaurantCard
            "movie" -> position
            else -> -1
        }
    }


    private fun bookmarkRecommendation() {

    }

    private fun recommendRecommendation() {

    }

    private fun notRecommendRecommendation() {

    }

    private fun shareRecommendation() {

    }

    private fun getScreenShot(view: View): Bitmap {
        val returnedBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) bgDrawable.draw(canvas)
        else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        return returnedBitmap
    }

}


open class RecommendationViewHolder(itemView : View): RecyclerView.ViewHolder(itemView) {

    open fun bind(recommendation: Recommendation, position: Int, context: Context?) {

    }
}
