package com.androidapp.recommendme.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.core.content.ContextCompat
import com.example.recommendme.R
import com.androidapp.recommendme.UserInterestAnswerOption
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.answer_option.view.*

class AnswerOptionAdapter(private val context:Context?, private val optionsList: List<UserInterestAnswerOption>): BaseAdapter() {

    private val inflater: LayoutInflater
            = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var selectedIndex:Int = -1

    fun setSelectedIndex(position: Int) {
        selectedIndex = position
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return optionsList.size
    }

    override fun getItem(position: Int): Any {
        return optionsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.answer_option, parent, false) as MaterialCardView
        val answerOption = getItem(position) as UserInterestAnswerOption

        rowView.answer_option_text.text = answerOption.text

        if (this.context != null) {
            if (selectedIndex != -1 && position == selectedIndex) {
                rowView.strokeColor =  ContextCompat.getColor(this.context, R.color.yellow)
            } else {
                rowView.strokeColor =  ContextCompat.getColor(this.context, R.color.answerOptionGrey)
            }
        }
        return rowView
    }
}