package com.androidapp.recommendme.adapters

import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.androidapp.recommendme.Recommendation
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.helpers.ResponseParser
import com.bumptech.glide.Glide
import com.example.recommendme.R
import com.facebook.AccessToken
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.bookmarks_item.view.*
import org.json.JSONObject

class BookmarksAdapter(val context: Context?, private var bookmarksList: MutableList<Recommendation>): BaseAdapter() {

    private val inflater: LayoutInflater
            = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var rowView = inflater.inflate(R.layout.bookmarks_item, parent, false) as MaterialCardView
        val recommendation = getItem(position) as Recommendation

        val movieRecommendation = ResponseParser().parseMovieDetails(recommendation.details_json)

        val tf1 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Demi.otf")
        val tf2 = Typeface.createFromAsset(context?.assets, "AvenirNextLTPro-Regular.otf")

        rowView.bookmark_title_text.text = movieRecommendation.title + " (" + movieRecommendation.releaseYear + ")"
        rowView.bookmark_title_text.typeface = tf1

        rowView.bookmark_detail_text.text = movieRecommendation.plot
        if (movieRecommendation.plot.length > 200) {
            rowView.bookmark_detail_text.text = movieRecommendation.plot.substring(0, 197) + "..."
        }
        rowView.bookmark_detail_text.typeface = tf2

        Glide.with(context)
            .load(movieRecommendation.primaryPhotoUrl)
            .into(rowView.bookmark_image)
            .waitForLayout()

        rowView.bookmarks_activity_bookmark_button.setOnClickListener {
            
        }

        return rowView
    }

    override fun getItem(position: Int): Any {
        return bookmarksList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return bookmarksList.size
    }

}