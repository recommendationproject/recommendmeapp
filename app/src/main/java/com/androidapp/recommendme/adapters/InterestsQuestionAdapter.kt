package com.androidapp.recommendme.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.interest_question.view.*
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidapp.recommendme.ServiceVolley
import com.androidapp.recommendme.User
import com.androidapp.recommendme.UserInterestAnswerOption
import com.androidapp.recommendme.UserInterestQuestion
import com.example.recommendme.*
import com.androidapp.recommendme.activities.RecommendationsActivity
import org.json.JSONArray
import org.json.JSONObject


class InterestsQuestionAdapter(val context: Context?, private var questions: MutableList<UserInterestQuestion>, private val layoutManager:LinearLayoutManager): RecyclerView.Adapter<InterestsQuestionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterestsQuestionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view: View

        view = inflater.inflate(R.layout.interest_question, parent, false)

        return InterestsQuestionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return questions.size
    }

    override fun onBindViewHolder(holder: InterestsQuestionViewHolder, position: Int) {
        val question: UserInterestQuestion = questions[position]
        var remaining_questions_count = questions.size - (position + 1)

        val answerOptions = question.options
        val answerOptionsAdapter =
            AnswerOptionAdapter(context, answerOptions)
        var selectedAnswer = UserInterestAnswerOption(
            text = "",
            property = "",
            isSelected = false
        )
        var answers = JSONArray()

        val intent = (context as Activity).intent
        val extras = intent.extras
        val userId = extras?.getString("userId")

        holder.itemView.answer_options_signup.setOnItemClickListener { _, _, position, _ ->
            answerOptionsAdapter.setSelectedIndex(position)
            selectedAnswer = answerOptions.get(position)
            answers.put(selectedAnswer.property)

            updateUserPersonalisationResponse(userId = userId,
                questionProperty = question.questionProperty,
                category = question.category,
                answers = answers)

            var endAction = object: Runnable {
                override fun run() {
                    layoutManager.scrollToPosition(holder.adapterPosition + 1)
                    holder.itemView.animate().setDuration(0).alpha(1.0f).start()
                }
            }
            if (holder.adapterPosition+1 < questions.size) {
                holder.itemView.animate().setDuration(1000).alpha(0.0f).withEndAction(endAction).start()
            }
        }

        holder.itemView.answer_options_signup.adapter = answerOptionsAdapter

        if (remaining_questions_count == 0) {
            holder.itemView.interests_coninue_button.visibility = View.VISIBLE
            holder.itemView.interests_coninue_button.setOnClickListener {
                if (context != null) {
                    var intent = Intent(context, RecommendationsActivity::class.java)
                    val currentActivityIntent = (context as Activity).intent
                    val extras = currentActivityIntent.extras
                    val userId = extras?.getString("userId")
                    val user = extras?.getSerializable("user") as User

                    intent.putExtra("userId", userId)
                    intent.putExtra("user", user)
                    startActivity(context,intent, Bundle.EMPTY)
                    context.finish()
                }
            }
        }

        holder.bind(questions, position, context)
    }

    private fun updateUserPersonalisationResponse(userId: String?, questionProperty: String, category: String, answers: JSONArray) {
        var params = JSONObject()
        params.put("question_property", questionProperty)
        params.put("category", category)
        params.put("answers", answers)

        ServiceVolley()
            .put("/user/personalisation?userid=" + userId, params) { response ->
            Log.d("USER_PERSONALISATION", response.toString())
        }
    }
}

class InterestsQuestionViewHolder(itemView : View): RecyclerView.ViewHolder(itemView) {

    fun bind(questions: List<UserInterestQuestion>, position: Int, context: Context?) {
        val question: UserInterestQuestion = questions[position]
        var remainingQuestionsCount = questions.size - (position + 1)

        itemView.interests_questions_progress_bar.progress = (position + 1) * 100 /questions.size
        itemView.question_title.text = question.questionText
        itemView.remaining_questions_text.text = remainingQuestionsCount.toString() + " more questions to go"

    }
}
