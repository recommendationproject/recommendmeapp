package com.androidapp.recommendme

import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

object Model {

}

data class UserLoginResponse (
    val isExistingUser: Boolean
)

data class User (
    var userId:String,
    var interestsCategories: List<String> = mutableListOf(),
    var firstName: String = "",
    var lastName: String = "",
    var email: String = "",
    var city: String = "",
    var facebookId: String = "",
    var profilePictureUrl: String = "",
    var answeredPersonalisationQuestions: List<UserInterestQuestion> = mutableListOf(),
    var movieProfile: MovieProfile = MovieProfile(),
    var isNewUser: Boolean = false,
    var remainingOnDemandMoviesCount: Int = 0,
    var onDemandMovieOptions: List<OnDemandMovieOption> = mutableListOf()
): Serializable

data class UserProfile (
    var firstName: String,
    var lastName: String,
    var email: String,
    var phoneNumber: String
)

data class Recommendation (
    var id:String = "",
    var category:String = "",
    var isBookmarked: Boolean = false,
    var title:String = "",
    var description:String = "",
    var details_json: JSONObject = JSONObject()
): Serializable

data class RecommendationFriendsInfo(
    var total_recommendations_count:Int = 0,
    var friend_picture_url:String = "",
    var friend_names: List<String> = mutableListOf()
): Serializable

data class RestaurantRecommendation (
    var title:String = "",
    var description:String = "",
    var location: RestaurantLocation = RestaurantLocation(),
    var highlights: String = "",
    var rating: String = "",
    var ratingCount: String = "",
    var ratingColor: String = "testColor",
    var deeplink: String = "",
    var url: String = "",
    var primaryPhotoUrl:String = "",
    var photos:List<String> = mutableListOf()
): Serializable

data class MovieRecommendation (
    var title:String = "",
    var description:String = "",
    var plot:String = "",
    var type:String = "",
    var language:String = "",
    var director:String = "",
    var writer:String = "",
    var cast: String = "",
    var genres: List<String> = mutableListOf(),
    var rating: String = "",
    var ratingCount: String = "",
    var deeplink: String = "",
    var url: String = "",
    var primaryPhotoUrl:String = "",
    var releaseYear:String = "",
    var trailer: String = "",
    var imdbId: String = "",
    var availableAtList: MutableList<Link> = mutableListOf(),
    var featuredInList: MutableList<Link> = mutableListOf()
): Serializable

data class RestaurantLocation (
    var address: String = "",
    var city: String = "",
    var latitude: String = "",
    var longitude: String = ""
): Serializable

data class UserInterestQuestion (
    var questionText: String,
    var questionProperty: String,
    var category: String,
    var options: List<UserInterestAnswerOption>
): Serializable

data class UserInterestAnswerOption (
    var text: String,
    var property: String,
    var isSelected: Boolean
): Serializable

data class CheckboxItem (
    var text: String,
    var property: String,
    var isSelected: Boolean
): Serializable

data class MovieProfile (
    var movieInterestRating: Int = 0,
    var genreList: MutableList<CheckboxItem>  = mutableListOf(),
    var languageList: MutableList<CheckboxItem> = mutableListOf(),
    var yearList: MutableList<CheckboxItem> = mutableListOf(),
    var streamingPlatformList: MutableList<String> = mutableListOf(),
    var sendSurpriseRecommendation: Boolean = true
): Serializable

data class OnDemandMovieOption (
    var tag: String = "",
    var text: String = ""
): Serializable

data class Categories (
    var categories: List<String>
): Serializable

data class Link (
    var displayText:String = "",
    var url: String = ""
): Serializable

object Supplier {

    val recommendations = listOf<Recommendation>(
        //Recommendation(category = "youtube", title = "Youtube Video",description = "Wow!! what a great video."),
        //Recommendation(category = "restaurant", title = "Restaurant", description = "Best food you'll ever eat.")
    )

    val questions = listOf<UserInterestQuestion>(/*
        UserInterestQuestion(question = "What is your dream restaurant?", options = listOf("Lit", "Dim")),
        UserInterestQuestion(question = "What kind of movies do you prefer watching?", options = listOf("Typical bollywood", "Sucker for foreign movies")),
        UserInterestQuestion(question = "What kind of videos do you prefer watching?", options = listOf("web series", "music videos"))*/
    )

}